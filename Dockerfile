FROM rocker/tidyverse:3.6.3-ubuntu18.04

# setup locales
ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Berlin
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# select default keyboard layout
RUN  echo "XKBMODEL='pc105'" >> /etc/default/keyboard && \
  echo "XKBLAYOUT='de'"  >> /etc/default/keyboard && \
  echo "XKBVARIANT=''"  >> /etc/default/keyboard && \
  echo "XKBOPTIONS=''"  >> /etc/default/keyboard && \
  echo "BACKSPACE='guess'"  >> /etc/default/keyboard


# install basic dependencies
RUN apt-get update && \
    apt-get -y install \
        keyboard-configuration console-setup \
        libgtk3-nocsd0 \
        gfortran cmake make \
        openssh-client openssh-server \
        docker.io \
        python3 python3-pip \
        curl zip unzip bzip2 \
        debootstrap libarchive-dev squashfs-tools \
        libtool m4 automake \
        nano vim git curl wget autoconf qpdf libtool libtool-bin \
        tcl tk tk-dev tk-table jags \
        perl perl-base default-jre openjdk-8-jre \
        samtools tophat hisat2 fastqc sortmerna sra-toolkit trimmomatic pandoc \
        libcurl4-gnutls-dev libgit2-dev libopenblas-dev libfontconfig1-dev libcairo2-dev libssh-dev libssh2-1-dev libbz2-dev liblzma-dev && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip3 install multiqc==1.7

# Fix repository to specific date
RUN mkdir -p /usr/lib/R/etc/ && \
    echo "r-cran-repos=https://packagemanager.rstudio.com/all/362" > /etc/rstudio/rsession.conf && \
    echo "options(repos = c(CRAN = 'https://packagemanager.rstudio.com/all/362', BIONIC = 'https://packagemanager.rstudio.com/all/__linux__/bionic/274'), download.file.method = 'libcurl')" > /usr/local/lib/R/etc/Rprofile.site && \
    echo "options(HTTPUserAgent = sprintf('R/%s R (%s)', getRversion(), paste(getRversion(), R.version$platform, R.version$arch, R.version$os)))" >> /usr/local/lib/R/etc/Rprofile.site && \
    echo "options(BioC_mirror = 'https://packagemanager.rstudio.com/bioconductor')" >> /usr/local/lib/R/etc/Rprofile.site && \
    cat /usr/local/lib/R/etc/Rprofile.site



# Install additional R packages a-priori
ENV R_LIBS_USER="/usr/local/lib/R/site-library"
RUN R --vanilla -e 'update.packages(ask=FALSE, Ncpus = max(2, min(10, parallel::detectCores())), repos = c("https://packagemanager.rstudio.com/all/362"))' &&\
    R --vanilla -e 'install.packages(c("BiocManager"), Ncpus = 3, repos = c("https://packagemanager.rstudio.com/all/362")); \
                    install.packages(c("tidyverse", "devtools", "pkgbuild", "magrittr", "readxl", "writexl"), Ncpus = max(2, min(10, parallel::detectCores())), repos = c("https://packagemanager.rstudio.com/all/362"))'
RUN R CMD javareconf

# Install GEO2RNAseq vignette dependencies
RUN R --vanilla -e "BiocManager::install(c('airway', 'DESeq', 'dendextend', 'GenomicFeatures', 'htmlwidgets', 'IRanges', 'GEOquery', 'TxDb.Dmelanogaster.UCSC.dm3.ensGene'), Ncpus = max(2, min(10, parallel::detectCores())))" && \
    R --vanilla -e 'devtools::install_version("knitr", version = "1.28", repos = c("https://packagemanager.rstudio.com/all/362"))'

# Install GEO2RNAseq from source
ADD . /source
WORKDIR /source
RUN R --vanilla -e 'devtools::install(".", upgrade="never", dependencies=TRUE, Ncpus = max(2, min(10, parallel::detectCores())), repos = c("https://packagemanager.rstudio.com/all/362"))'



SHELL ["/bin/bash", "-c"]
EXPOSE 8787
WORKDIR /analysis
CMD ["/init"]
