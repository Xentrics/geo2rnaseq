library(ShortRead)

dat <- c("data/fastq/raw1_1.fastq", "data/fastq/raw1_2.fastq") #<-- fill in!
f1_sample <- sample(c(T,F), size = 100000, rep = 100000)
f2_sample <- sample(c(T,F), size = 100000, rep = 100000)
f1_str <- FastqSampler(dat[1], n = 1e5)
f2_str <- FastqSampler(dat[2], n = 1e5)
f1_fq <- yield(f1_str)
f2_fq <- yield(f2_str)

target_sync <- sum(f1_sample & f2_sample)
rem1 <- 100000 - sum(f1_sample)
rem2 <- 100000 - sum(f2_sample)

dir.create("test")
writeFastq(f1_fq[f1_sample], "test/filter1.fastq", mode = "w", compress = F)
writeFastq(f2_fq[f2_sample], "test/filter2.fastq", mode = "w", compress = F)
res <- synchronize_fastq(dat[1], "test/filter1.fastq", "test/filter2.fastq")