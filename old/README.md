# README #
Archive for old versions of the RNA-seq pipeline R-scripts.

Could be useful to see how the pipeline evolved or if an important part of the code "got lost" in newer versions.

* **.R** old version from J�rg Linde, possibly modified by Stefan Pietsch
* **-SS_2015-Aug-20.R**: latest(?) version from Sylvie Schulze; not going to be updated again
* **_TW.R**: old version from Thomas Wolf, based on J�rg and Sylvies versions
* **_TW.R.old**: even older version from Thomas Wolf, based on J�rg and Sylvies versions

