# parallel unzip of zip-file with piz
unzip = function(rawfastqfiles, folder, cpus=1) {
  unzipdriver = function(i) {
    system(paste("pigz", "-d", rawfastqfiles[i]))
    rawfastqfiles[i] = substr(rawfastqfiles[i], 1, nchar(rawfastqfiles[i]) - 3)
  }
  return(mclapply(1:length(rawfastqfiles), unzipdriver, mc.cores = cpus))
}

# parallel unzip of bz2-files
bunzip = function(rawfastqfiles, folder, cpus=1) {
  unzipdriver = function(i) {
    system(paste("bunzip2",  rawfastqfiles[i]))
    rawfastqfiles[i] = substr(rawfastqfiles[i], 1, nchar(rawfastqfiles[i]) - 4)
  }
  return(mclapply(1:length(rawfastqfiles), unzipdriver, mc.cores = cpus))
}

# TODO file.path
# TODO paste
# htseq is super slow
htseq_par = function(path,
                     targetfolder,
                     gff,
                     mode = "union",
                     stranded = "no",
                     featuretype =
                       "exon",
                     idattr = "gene_id",
                     cpus = 1) {
  pardriver = function(i) {
    targetfile = paste0(targetfolder,
                        "/",
                        strsplit(filesshort[i], "\\.")[[1]][[1]],
                        ".csv")
    warnings = paste0(targetfolder,
                      "/",
                      strsplit(filesshort[i], "\\.")[[1]][[1]],
                      "_warnings.txt")

    system(
      paste(
        "htseq-count -m",
        mode,
        "-s",
        stranded,
        "-t ",
        featuretype,
        "-i",
        idattr,
        files[i],
        gff,
        " >",
        targetfile ,
        " 2>",
        warnings
      )
    )

  }
  if (!(file.exists(targetfolder)))
    system(paste0("mkdir ", targetfolder))
  filesshort = list.files(path, "")
  filesshort = filesshort[grep("sam", filesshort)]
  files = paste(path, filesshort, sep = "/")

  parres = mclapply(1:length(files), pardriver, mc.cores = cpus)
}


merge2 = function(x, y, ...) {
  x = as.data.frame(x)

  y = as.data.frame(y)

  x[, ncol(x) + 1] = rownames(x)

  y[, ncol(y) + 1] = rownames(y)

  colnames(x)[ncol(x)] = colnames(y)[ncol(y)] = "key"

  combi = merge(x, y, by = "key", ...)

  rownames(combi) = combi$key

  combi[,-1]
}


merge.by.names = function(x, y, ...) {
  x = cbind(x, names(x))
  y = cbind(y, names(y))
  colnames(x)[ncol(x)] = colnames(y)[ncol(y)] = "key"

  combi = merge(x, y, by = "key", ...)

  rownames(combi) = combi$key

  return(combi[,-1])
}


# TODO paste
paired_btrimdriver = function(i, windowsizetrimming, qualcuttrimming) {
  system(
    paste(
      "Btrim64",
      "-t",
      pairedfiles[i, 1],
      "-o",
      paste0(pairedfiles[i, 1], ".bt"),
      "-S -q  -w ",
      windowsizetrimming,
      "-a" ,
      qualcuttrimming ,
      "-s",
      paste0(pairedfiles[i, 1], ".bt", ".sum")
    )
  )
  system(
    paste(
      "Btrim64 -t",
      pairedfiles[i, 2],
      "-o",
      paste0(pairedfiles[i, 2], ".bt"),
      "-S -q  -w ",
      windowsizetrimming,
      "-a" ,
      qualcuttrimming ,
      "-s",
      paste0(pairedfiles[i, 2], ".bt", ".sum")
    )
  )
  #synchronize trimming of both pairs
  system(
    paste(
      "perl /home/jlinde/Scripts/paired_end_trim.pl",
      # TODO global alternative?
      paste0(pairedfiles[i, 1], ".bt", ".sum"),
      paste0(pairedfiles[i, 2], ".bt", ".sum"),
      paste0(pairedfiles[i, 1], ".bt"),
      paste0(pairedfiles[i, 2], ".bt") ,
      sep = " "
    )
  )
  pairedfiles[i,] = c(paste0(pairedfiles[i, 1], ".bt.pe"),
                      paste0(pairedfiles[i, 2], ".bt.pe"))

}


# TODO file.path
# TODO paste
# TODO folder
sam_to_bam = function(files, bamfolder, samfolder, cpus = 1) {
  driver = function(i, bamfolder, samfolder) {
    tmp = shortfiles(files[i])
    tmp2 = paste0(tmp, ".bam")
    system(paste0("samtools view -bS ",
                  samfolder,
                  tmp,
                  " > ",
                  bamfolder,
                  tmp2))
    return(paste0(bamfolder, tmp2))
  }
  res = mclapply(1:length(files), driver, bamfolder, samfolder, mc.cores = cpus)
  return(res)
}


# TODO nicht für paket, aber für funktionssammlung
# TODO file.path
# TODO paste
# TODO folder
# split combined counting files in pathogen_counting and host_counting file
split_dualBamFiles =
  function(bamfiles,
           pathogenID,
           hostID,
           CPUS = length(bamfiles),
           folder =
             "Bamfolder",
           Bamfolder1 = "BamfilesHost",
           Bamfolder2 = "BamfilesPathogen") {
    pathogenID = paste0(".REF_", pathogenID, ".bam")
    hostID = paste0(".REF_", hostID, ".bam")
    system(paste0("mkdir " , folder, "/", Bamfolder1))
    system(paste0("mkdir " , folder, "/", Bamfolder2))

    for (i in 1:length(bamfiles)) {
      ##SHOULD BETTER BE PARALISED. TAKE CARE SEVERAL CORES USE THE SAME TMPFILE
      cat(paste("running for file", bamfiles[i], sep = " "))
      system(paste("bamtools split -in", bamfiles[i], "-reference", sep =
                     " "))
      tmp = paste0(substr(bamfiles[i], 1, nchar(bamfiles[i]) - 4), pathogenID)
      tmp = intersect(shortfiles(tmp), list.files("Bamfolder/", substr(
        shortfiles(bamfiles[i]), 1, nchar(shortfiles(bamfiles[i])) - 4
      )))
      tmp = paste(folder, tmp, sep = "/")

      write.table(
        tmp,
        "tmpfile",
        quote = F,
        col.names = F,
        row.names = F
      )
      system(
        paste(
          "samtools merge -f -b  ",
          "tmpfile ",
          folder,
          "/",
          Bamfolder2,
          "/",
          shortfiles(bamfiles[i]),
          sep =
            ""
        )
      )
      system(paste(
        "samtools index ",
        folder,
        "/",
        Bamfolder2,
        "/",
        shortfiles(bamfiles[i]),
        sep =
          ""
      ))
      system("rm tmpfile")

      tmp = paste(substr(bamfiles[i], 1, nchar(bamfiles[i]) - 4), hostID, sep =
                    "")
      tmp = intersect(shortfiles(tmp), list.files("Bamfolder/", substr(
        shortfiles(bamfiles[i]), 1, nchar(shortfiles(bamfiles[i])) - 4
      )))
      tmp = paste(folder, tmp, sep = "/")
      write.table(
        tmp,
        "tmpfile",
        quote = F,
        col.names = F,
        row.names = F
      )
      system(
        paste(
          "samtools merge -f -b ",
          "tmpfile ",
          folder,
          "/",
          Bamfolder1,
          "/",
          shortfiles(bamfiles[i]),
          sep =
            ""
        )
      )
      system(paste0(
        "samtools index ",
        folder,
        "/",
        Bamfolder1,
        "/",
        shortfiles(bamfiles[i])
      ))
      system("rm tmpfile")
      system(paste0("rm " , folder, "/", "*REF*"))
    }
    a = list.files(paste0(folder, "/", Bamfolder1))
    a = a[-grep("bai", a)]
    a = paste(folder, Bamfolder1, a, sep = "/")
    b = list.files(paste0(folder, "/", Bamfolder2))
    b = b[-grep("bai", b)]
    b = paste(folder, Bamfolder2, b, sep = "/")
    system(paste0("rm " , folder, "/", "*REF*"))
    system("rm tmpfile")

    return(res = list(a, b))
  }
