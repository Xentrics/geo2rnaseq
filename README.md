# GEO2RNAseq: An easy-to-use R pipeline for complete pre-processing of RNA-seq data #

![https://opensource.org/licenses/GPL-3.0](https://img.shields.io/badge/license-GPLv3-blue.svg)  
![https://hub.docker.com/repository/docker/xentrics/geo2rnaseq](https://img.shields.io/docker/pulls/xentrics/geo2rnaseq.svg)  
![https://hub.docker.com/repository/docker/xentrics/geo2rnaseq](https://img.shields.io/docker/automated/xentrics/geo2rnaseq.svg)  


RNA-seq pipeline developed in the research group [Systems Biology and Bioinformatics](http://www.leibniz-hki.de/en/systembiologie-und-bioinformatik.html) at [HKI](http://www.leibniz-hki.de/en/home.html).


Cite: [https://doi.org/10.1101/771063 ](bioRxiv)

---

# Installation / Run
* GEO2RNAseq is available in
    * [Docker-Hub](https://hub.docker.com/repository/docker/xentrics/geo2rnaseq)
    * [Singularity-Hub](https://singularity-hub.org/collections/4387)
    * [Source on bitbucket](https://bitbucket.org/Xentrics/geo2rnaseq/)
* Running vom **Docker** is the most easy way to use
    * it will run the R-studio server as supplied by [rocker/rstudio](https://hub.docker.com/r/rocker/rstudio)
        1. run the docker commands **below**
        2. in your browser, go to **localhost:8787**
        3. login as **rstudio** with _the password you chose in the command_


**Minimum docker commands**
```shell
docker pull xentrics/geo2rnaseq
docker run -d -p 8787:8787 -v $(pwd):/analysis -e PASSWORD=yourpasswordhere xentrics/geo2rnaseq
```


If you want to keep you current user ID and Group IDs for output files, the docker needs to be started differently:
```shell
docker run -d \
    -e PASSWORD=yourpasswordhere \
    -p :8787:8787 \
    -v $(pwd):/analysis \
    xentrics/geo2rnaseq \
    bash -c "groupadd -g $(id -g) GROUP ; \
            usermod -u $(id -u) -g $(id -g) -d /analysis rstudio && \
            /init"
```

---



# Dev Version

* **rRNA databases** for **SortMeRNA** are to big to directly include them in this repository
* These need to be build externally, or downloaded from ![here](https://bitbucket.org/Xentrics/geo2rnaseq/downloads/rRNA_db.tar.xz)
    * _this database is created by merging all rRNA FASTA files to speed up rRNA filtering_
* Otherwise, rRNA FASTA files should be acquired from SILVA and extracted into one directory
* These can then be indixed using Docker. GEO2RNAseq comes with a function called 'run_sortmerna_indexer' that takes care of this task
If you want to keep you current user ID and Group IDs for output files, the docker needs to be started differently:
```shell
docker run -d \
    -e PASSWORD=yourpasswordhere \
    -p :8787:8787 \
    -v $(pwd):/analysis \
    -v rRNA_databases:/rRNA_databases \
    -v rRNA_index:/rRNA_index \
    xentrics/geo2rnaseq \
    bash -c "groupadd -g $(id -g) GROUP ; \
            usermod -u $(id -u) -g $(id -g) -d /analysis rstudio && \
            /init"
```


---

# Features #
* processing of RNA-seq raw data
    * for single RNA-seq, dual RNA-seq, and triple RNA-seq, e.g. host-pathogen interactions
    * for single-end, paired-end and mixed SE/PE sequencing
* for Linux and Mac
* [Conda](https://anaconda.org/xentrics/r-geo2rnaseq) package

# Steps (most are optional) #
* get raw data and metadata from GEO
* quality control
* quality and adapter trimming
* rRNA removal
* mapping
* counting
* mapping stats
* clustering, PCA
* DEG analysis with multiple tools
* visualization for each step


# Important Files and Folders #
* **R/** collection of functions
    * needed by the pipeline scripts
    * run `R.utils::sourceDirectory("R/")` to use GEO2RNA-seq without actually installing the package
* **inst/extdata/pipelines/** actual RNA-seq pipelines for different types of datasets, application examples
* **inst/extdata/BASH/** shell scripts to download raw and metadata from GEO
* **inst/extdata/multiqc_config.yaml** [MultiQC](http://multiqc.info/) example config file
* **inst/doc/** detailed documentation (R vignette)



## Enhancing packages
```R
biocLite(c(
    "airway",
    "IRanges",
    "TxDb.Dmelanogaster.UCSC.dm3.ensGene",
    "GEOquery",
    "pasillaBamSubset",
    "SRAdb"
))

install.packages(c(
    "dendextend",
    "roxygen2"
))
```

# Contact #
[bastian.seelbinder@leibniz-hki.de](mailto:bastian.seelbinder@leibniz-hki.de)
