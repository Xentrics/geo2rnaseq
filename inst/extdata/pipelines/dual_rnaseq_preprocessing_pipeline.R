# R320

# load libraries and import functions ##########################################
library("Geo2RNAseq")
# R.utils::sourceDirectory("R/") # only if not installed via install.packages()

# get info #####################################################################

# first, export SDRF sheet of Excel template in CSV format
# NOTE: parse_meta_xls can be used for SDRF.xls files.
#       Make sure to use the correct column name! ~> Library.Strategy
# second, read entire file
SDRF_file <- "xxx" # <-- fill in
SDRF_info <- parse_SDRF(SDRF_file, skip = 8, col.name.row = 7)
# SDRF_info <- parse_meta_xls(SDRF_file, "Library.Strategy")

SDRF           <- SDRF_info$table
number_samples <- SDRF_info$nrSamples
samples        <- SDRF_info$samples
design_matrix  <- SDRF_info$designMatrix

# alternative: automatically create design matrix based on metadata
# design_matrix <- createDesignMatrix(metadata_dataframe)

# global settings ##############################################################

# output directories
fastqDir <- "./fastq"
qualDir  <- "./quality"
mapDir   <- "./mapping"
countDir <- "./counting"
tabDir   <- "./result_tables"
plotDir   <- "./result_plots"
degDir   <- "./diff_exp_genes"

MAX_CPUS <- 30 # allocate no more than <int> CPUs <-- fill in!

# If TRUE, existing files will be overwritten without questioning.
# If FALSE, most methods will skip step for existing output files.
# In these cases, the method will return 'call[x] = "NOT USED"'
FORCE_OVERWRITE <- FALSE # <-- fill in!

# fill in!
# gene ids start with ...
geneID_sp1 <- "ENSG"         # sp1 = species 1 = host     = H. sapiens
geneID_sp2 <- "C[1234567MR]" # sp2 = species 2 = pathogen = C. albicans

name_sp1 <- "Hsapiens"
name_sp2 <- "Calbicans"

name_sp1 <- gsub("\\W", "_", name_sp1)
name_sp2 <- gsub("\\W", "_", name_sp2)

# paired or unpaired reads?
paired <- logical()
if (grepl("paired", SDRF$Library.Strategy[1], ignore.case = T)) {
    paired <- TRUE
    paired_files <- matrix(NA, nrow = nrow(SDRF), ncol = 2)
} else if (grepl("single", SDRF$Library.Strategy[1], ignore.case = T)) {
    paired <- FALSE
} else {
    stop("Cannot figure out if SDRF$Library.Strategy is single- or paired-end")
}

# files ########################################################################

# sp1 = species 1
# sp2 = species 2
# comb = combined = sp1+sp2

# caveat: TopHat2/HISAT2 are looking for the genome file in the index directory
# move it there or create a link if the genome is located in its own directory
# fill in!
genome_sp1  <- "xxx"
genome_sp2  <- "xxx"
genome_comb <- "xxx"
# TODO add function to combine files

# <gene_id "something";> must be first attribute
anno_sp1  <- "xxx"
anno_sp2  <- "xxx"
anno_comb <- "xxx"
# TODO add function to combine files

# build index with make_HiSat2_index() or make_Tophat_index()
index_comb <- "xxx"

# fill in!
# update SDRF metadata
SDRF$Genome.Annotation.Source    <- paste("Ensembl", "CGDB", sep = "; ")
SDRF$Genome.Annotation.Version   <- paste("GRCh38_89", "A22-s07-m01-r29", sep = "; ")
SDRF$Genome.Annotation.Date      <- paste("2017-05-07", "2017-06-04", sep = "; ")
SDRF$Genome.Annotation.Data.File <- genome_comb

SDRF$Structural.Annotation.Source    <- paste("Ensembl", "CGDB", sep = "; ")
SDRF$Structural.Annotation.Version   <- paste("GRCh38_89", "A22-s07-m01-r29", sep = "; ")
SDRF$Structural.Annotation.Date      <- paste("2017-05-07", "2017-06-04", sep = "; ")
SDRF$Structural.Annotation.Data.File <- anno_comb

# raw FASTQ files ##############################################################

### testing shortcut to populate raw_fastq_files ###
# raw_fastq_files <- list.files("fastq", full.names = TRUE)
# raw_fastq_files <- raw_fastq_files[grep("md5", raw_fastq_files, invert = TRUE)]
### testing shortcut to populate raw_fastq_files ###

if (paired) {
    raw_fastq_files <- SDRF_info$peFiles
} else {
    raw_fastq_files <- SDRF_info$seFiles
}

if (!file.exists(raw_fastq_files[1]))
    raw_fastq_files <- file.path(fastqDir, raw_fastq_files)  # extent to full path

## 1.1 Decompress - optional
decompress <- FALSE
endings <- sapply(raw_fastq_files, function(x){s <- unlist(strsplit(x, "\\.")); s[length(s)]})
knownEndings <- c("gz", "bz2", "zip", "fastq")
# check if all files are compressed
if (decompress && FALSE %in% (endings %in% knownEndings)) stop("Cannot decompress. One or more files have unknown file extension.")

if (decompress) {
    if (length(dir(fastqDir)) > 0)
        warning(paste0("Directory \"", fastqDir, "\" not empty! Overwrite? ", FORCE_OVERWRITE), immediate. = TRUE)
    if (FORCE_OVERWRITE || length(dir(fastqDir)) == 0) {
        writeLines("Decompressing FASTQ files ...")
        raw_fastq_files <- unzip(raw_fastq_files, dir = fastqDir)
    } else {
        raw_fastq_files <- list.files(fastqDir, pattern = "[^pe|se|non_rrna]\\.fastq$", full.names=TRUE)
        raw_fastq_files <- grep("\\.merged\\.", raw_fastq_files, invert = TRUE, value = TRUE)
    }
} else {
    # NOTE: Experimental
    raw_fastq_files <- list.files(fastqDir, pattern = "[^pe|se|non_rrna]\\.fastq$", full.names=TRUE)
    raw_fastq_files <- grep("\\.merged\\.", raw_fastq_files, invert = TRUE, value = TRUE)
}

# for PE, we have twice the number of files
stopifnot(length(raw_fastq_files) == number_samples + number_samples*paired)

writeLines("Working with files:")
print(raw_fastq_files)

# quality control 1 (before trimming) ##########################################

qualityRawDir <- file.path(qualDir, "raw")
if (length(dir(qualityRawDir)) > 0)
    warning(paste0("Directory \"", qualityRawDir, "\" not empty! Overwrite? ", FORCE_OVERWRITE), immediate. = TRUE)

if (FORCE_OVERWRITE || length(dir(qualityRawDir)) == 0)
{
    writeLines("FastQC - raw ...")
    fq_res <- run_FastQC(raw_fastq_files, outDir = qualityRawDir, cpus = MAX_CPUS, extend = T)

    # update SDRF metadata
    rows <- which(fq_res$calls != "NOT USED")
    if (paired) rows <- rows[1:(length(rows)/2)]

    col <- "Data.Processing.Software"
    SDRF[rows, col] <- "FastQC" # according to FungiNet Lookup-Table
    col <- "Description"
    SDRF[rows, col] <- fq_res$version # version of tool

    col <- "Parameters.And.Values"
    if (paired) {
        SDRF[rows, col] <- paste0(
            fq_res$calls[seq(1, length(fq_res$calls), by = 2)],
            "; ",
            fq_res$calls[seq(2, length(fq_res$calls), by = 2)]
        )
    } else {
        SDRF[rows, col] <- fq_res$calls # complete call
    }
}

# trimming #####################################################################

trimmedDir <- fastqDir
windowsizetrimming <- 15
qualcuttrimming <- 25
phred <- "-phred33"
leading <- 3
trailing <- 3
minlen <- 30

if (length(list.files(trimmedDir, pattern = "\\.trimo(\\.pe)*\\.fastq$")) > 0)
    warning(paste0("Directory \"", trimmedDir, "\" not empty! Overwrite? ", FORCE_OVERWRITE), immediate. = TRUE)

trimming_res <- run_Trimmomatic(
    files      = raw_fastq_files,
    is.paired  = paired,
    outDir     = trimmedDir,
    cpus       = MAX_CPUS,
    windowsize = windowsizetrimming,
    qualcut    = qualcuttrimming,
    phred      = phred,
    leading    = leading,
    trailing   = trailing,
    minlen     = minlen
)

# number of reads after trimming based on Trimmomatic output
number_raw     <- trimming_res$input
number_trimmed <- trimming_res$surviving

if (paired) {
    trimmed_fastq_files <- asPairVector(trimming_res$files)
} else {
    trimmed_fastq_files <- trimming_res$files
}

# sanity check. Can be ignored during experimental sessions
stopifnot(length(trimmed_fastq_files) == number_samples * (paired+1))

names(number_raw)     <- basename(raw_fastq_files)
names(number_trimmed) <- basename(trimmed_fastq_files)

fastq_files <- trimmed_fastq_files

# update SDRF metadata
software_nr <- 1
rows <- which(trimming_res$calls != "NOT USED")
col <- paste0("Data.Processing.Software.", software_nr)
SDRF[rows, col] <- "trimmomatic" # according to FungiNet Lookup-Table
col <- paste0("Description.", software_nr)
SDRF[rows, col] <- basename(sub(".*/(.+)\\.jar.*", "\\1", trimming_res$calls[1])) # name and version of tool
col <- paste0("Parameters.And.Values.", software_nr)
SDRF[rows, col] <- trimming_res$calls # complete call

# SortMeRNA --> remove rRNA reads ##############################################

filterrRNA <- TRUE
sortmeDir <- file.path(fastqDir, "sortmerna")

if (filterrRNA && length(dir(sortmeDir)) > 0)
    warning(paste0("Directory \"", sortmeDir, "\" not empty! Overwrite? ", FORCE_OVERWRITE), immediate. = TRUE)

if (filterrRNA) {
    sortmerna_res <- run_SortMeRNA(
        fastq_files,
        outDir    = fastqDir,
        mode      = "fast",
        paired    = paired,
        cpus      = MAX_CPUS
    )
    non_rrna_files <- sortmerna_res$files
    number_nonrRNA <- sortmerna_res$nonrrna

#     TODO right now, there is no column for this step (SortMeRNA)
#     in the FungiNet template. The current implementation would occupy
#     the columns of the next preprocessing step.
#     Either update the template or do not write this step to the template file.
#
#     # update SDRF metadata
#     software_nr <- software_nr + 1
#     rows <- which(sortmerna_res$calls != "NOT USED")
#     col <- paste0("Data.Processing.Software.", software_nr)
#     SDRF[rows, col] <- "SortMeRNA" # according to FungiNet Lookup-Table
#     col <- paste0("Description.", software_nr)
#     SDRF[rows, col] <- sortmerna_res$version
#     col <- paste0("Parameters.And.Values.", software_nr)
#     SDRF[rows, col] <- sortmerna_res$calls
}

# IF sample/file order changed!
# order(trimmed_fastq_files) != order(non_rrna_files)
# trimmed: old order 1,2,3,...
# non_rrna: new order 10,1,20,2,...
fastq_files <- if (filterrRNA) non_rrna_files else trimmed_fastq_files

# quality control 2 (after trimming) ###########################################

qualitySubDir <- if (filterrRNA) file.path(qualDir, "non_rrna") else file.path(qualDir, "trimmed")
if (length(dir(qualitySubDir)) > 0)
    warning(paste0("Directory \"" , qualitySubDir, "\" not empty! overwrite? ", FORCE_OVERWRITE))

if (FORCE_OVERWRITE || length(dir(qualitySubDir)) == 0)
{
    writeLines("FastQC - trimmed ...")
    fq_res <- run_FastQC(fastq_files, outDir = qualitySubDir, cpus = MAX_CPUS, extend = F)

#     TODO right now, there is no column for this step (FastQC after trimming)
#     in the FungiNet template. The current implementation would occupy
#     the columns of the next preprocessing step.
#     Either update the template or do not write this step to the template file.
#
#     # update SDRF metadata
#     software_nr <- software_nr + 1
#     rows <- which(fq_res$calls != "NOT USED")
#     col <- paste0("Data.Processing.Software.", software_nr)
#     SDRF[rows, col] <- "FastQC" # according to FungiNet Lookup-Table
#     col <- paste0("Description.", software_nr)
#     SDRF[rows, col] <- fq_res$version # version of tool
#
#     col <- paste0("Parameters.And.Values.", software_nr)
#     if (paired) {
#         SDRF[rows, col] <- paste0(
#             fq_res$calls[seq(1, length(fq_res$calls), by = 2)],
#             "; ",
#             fq_res$calls[seq(2, length(fq_res$calls), by = 2)]
#         )
#     } else {
#         SDRF[rows, col] <- fq_res$calls # complete call
#     }
}

# mapping ######################################################################

index_map <- index_comb
anno_map  <- anno_comb

mapper <- "hisat2"
bamDir <- file.path(mapDir, "bamfiles")
samDir <- file.path(mapDir, "samfiles")
convert_sam <- TRUE # should SAM files be converted to BAM files if BAM files are not found?

if (mapper == "tophat2")
{
    mapping_res <- run_Tophat(
        fastq_files,
        index        = index_map,
        outDir       = mapDir,
        is.paired    = paired,
        anno         = anno_map,
        addArgs      = NA,      # "-g 2 --b2-very-sensitive --no-coverage-search",
        cpus         = MAX_CPUS,
        worker       = if (paired) 5 else 10,
        use.existing = T
    )
    bam_files <- mapping_res$files

    # update SDRF metadata
    software_nr <- software_nr + 1
    rows <- which(mapping_res$calls != "NOT USED")
    col <- paste0("Data.Processing.Software.", software_nr)
    SDRF[rows, col] <- "tophat2" # according to FungiNet Lookup-Table
    col <- paste0("Description.", software_nr)
    SDRF[rows, col] <- mapping_res$version
    col <- paste0("Parameters.And.Values.", software_nr)
    SDRF[rows, col] <- mapping_res$calls
} else {

    if (FORCE_OVERWRITE || length(dir(samDir)) == 0)
    {
        mapping_res <- run_Hisat2(
            files     = fastq_files,
            index     = index_map,
            outDir    = mapDir,
            is.paired = paired,
            addArgs   = "",
            splice    = T,
            cpus      = MAX_CPUS
        )

    ## NOTE: use that only if the run_Hisat2() parameter 'as.bam' is FALSE.
    # bam_files <- sam_to_bam(mapping_res$mapFiles, sort=TRUE, bamDir = bamDir)
    bam_files <- mapping_res$files

    # update SDRF metadata
    software_nr <- software_nr + 1
    rows <- which(mapping_res$calls != "NOT USED")
    col <- paste0("Data.Processing.Software.", software_nr)
    SDRF[rows, col] <- "HiSat2" # according to FungiNet Lookup-Table
    col <- paste0("Description.", software_nr)
    SDRF[rows, col] <- mapping_res$version
    col <- paste0("Parameters.And.Values.", software_nr)
    SDRF[rows, col] <- mapping_res$calls

    } else {
        if (convert_sam) {
            sam_files <- list.files(samDir, pattern = "\\.sam$", full.names = TRUE)
            if (length(sam_files) != number_samples)
                stop(paste("Invalid number of SAM files. Expected", number_samples, "but got", length(sam_files)))
            bam_files <- sam_to_bam(mapping_res$mapFiles, sort=TRUE, bamDir = bamDir)
        } else
            bam_files <- list.files(bamDir, pattern = "\\.bam$", full.names = TRUE)

        if (length(bam_files) != number_samples)
            warning(paste("Found", length(bam_files), "BAM files, but expected", number_samples, ". Maybe SAMtools was interrupted. Try to convert again? ", convert_sam), immediate. = TRUE)
    }
}

## TODO can be remove, because already part of calc_dual_mapping_stats()?
# get chromosome names
chromosomeNames_sp1 <- system(
    paste("perl", "-ne", "'if (/^>(\\S+)/) { print \"$1\n\" }'", genome_sp1),
    intern = TRUE
)
chromosomeNames_sp2 <- system(
    paste("perl", "-ne", "'if (/^>(\\S+)/) { print \"$1\n\" }'", genome_sp2),
    intern = TRUE
)

## TODO can be remove, because already part of calc_dual_mapping_stats()?
# remaining chromosomes are sp2
number_combinedMapping <- stats_combinedMapping(
    files       = bam_files,
    chromosomes = chromosomeNames_sp1,
    cpus        = MAX_CPUS
)
names(number_combinedMapping) <- basename(bam_files)
number_combinedMapping <- unlist(number_combinedMapping)

# counting #####################################################################

anno_count <- anno_comb

if (length(dir(countDir)) > 0)
    warning(paste("Directory", countDir, "not empty! Overwrite?", FORCE_OVERWRITE), immediate. = TRUE)

if (FORCE_OVERWRITE || length(dir(countDir)) == 0)
{
    res_counting <- run_featureCounts(
        files             = bam_files,
        annotation        = anno_count,
        isGTF             = TRUE,
        IDtype            = "gene_id",
        featureType       = "exon",
        outDir            = countDir,
        isPairedEnd       = paired,
        allowMultiOverlap = FALSE,
        cpus              = MAX_CPUS
    )

    # update SDRF metadata
    software_nr <- software_nr + 1
    rows <- which(res_counting$calls != "NOT USED")
    col <- paste0("Data.Processing.Software.", software_nr)
    SDRF[rows, col] <- "featureCounts" #  according to FungiNet Lookup-Table
    col <- paste0("Description.", software_nr)
    SDRF[rows, col] <- res_counting$version
    col <- paste0("Parameters.And.Values.", software_nr)
    SDRF[rows, col] <- res_counting$calls
} else {
    res_counting <- read.featureCounts.files(countDir)
}

## TODO can be removed? Already part of run_featureCounts()?
# for dual, the counting variables need additional processing
# generate count file for each FASTQ/BAM/SAM file
# (needed for MultiQCs featureCounts report)
for (i in 1:nrow(res_counting$summary)) {
    # add "Status" line to beginning of table
    table <- c(rownames(res_counting$summary)[i], res_counting$summary[i, ])
    names(table) <- c("Status", names(res_counting$summary[i, ]))

    # write table to file
    write.table(
        table,
        file = file.path(countDir, paste0(table[1], ".counts.summary")),
        sep = "\t",
        quote = FALSE,
        col.names = FALSE
    )
}

counts <- res_counting$counts
countfile <- res_counting$countFile
sumfile <- res_counting$sumFile
lib_sizes <- res_counting$summary[,1]

## counting files from dual samples in single files
# counting_files_sp1  <- paste(countfile, "_sp1")
# list.files("counting")[grep("sp1$", list.files("counting"))] # will return wrong order of files!
# counting_files_sp2  <- paste0(countfile, "_sp2")
# list.files("counting")[grep("sp2$", list.files("counting"))] # will return wrong order of files!

## combine count files per sample into single count file
# counts_sp1 <- generateCountFile(basename(counting_files_sp1))
# colnames(counts_sp1) <- samples
# counts_sp2 <- generateCountFile(basename(counting_files_sp2))
# colnames(counts_sp2) <- samples

counts_sp1 <- counts[grep(geneID_sp1, rownames(counts)), ]
counts_sp2 <- counts[grep(geneID_sp2, rownames(counts)), ]

colnames(counts_sp1) <- samples
colnames(counts_sp2) <- samples

# get gene lengths from featureCounts result
gene_length_sp1 <- res_counting$anno$Length[grep(geneID_sp1, res_counting$anno$GeneID)]
gene_length_sp2 <- res_counting$anno$Length[grep(geneID_sp2, res_counting$anno$GeneID)]

names(gene_length_sp1) <- res_counting$anno$GeneID[grep(geneID_sp1, res_counting$anno$GeneID)]
names(gene_length_sp2) <- res_counting$anno$GeneID[grep(geneID_sp2, res_counting$anno$GeneID)]

# optional: assambly A22 of C. albicans only
# A22_to_A21 <- read.table(
#     "xxx.tab", # <-- fill in! (table with A22_ID <-> A21_ID mapping)
#     sep   = "\t",
#     as.is = TRUE
# )
# merged <- .merge_diploid(
#     counts       = counts_sp2,
#     mapping      = A22_to_A21,
#     allele_regex = "_[A|B]$",
#     gene_length  = gene_length_sp2
# )
# counts_sp2 <- merged$counts
# gene_length_sp2 <- merged$gene_length

# write counts to CSV file
write_count_table(
    file = file.path(tabDir, paste0(name_sp1, "_counts")),
    counts = counts_sp1
)
write_count_table(
    file = file.path(tabDir, paste0(name_sp2, "_counts")),
    counts = counts_sp2
)

# write counts to XLS file
write_count_table(
    file       = file.path(tabDir, paste0(name_sp1, "_counts")),
    counts     = counts_sp1,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp2, "_counts")),
    counts     = counts_sp2,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)

# don't need to compute libsizes, genelength and that stuff in the dual case
# at least not now/here

# check for rRNA contamination #################################################

detect_high_coverage(counts_sp1)
detect_high_coverage(counts_sp2)

# SAMtools #####################################################################

flagstatDir <- file.path(mapDir, "flagstats")
# TODO: we could use idx stats now
# bam_files <- sortBAMs(bam_files, overwrite = FORCE_OVERWRITE, delete.orig = F)
flag_files <- make_flagstats(bam_files, flagstatDir, MAX_CPUS)

# MultiQC ######################################################################
# TODO run some modules for sp1 and sp1 separately?
# TODO: find location automatically?
multiqc_config <- "./multiqc_config.yaml" # <-- fill in! (example in inst/extdata/)

run_MultiQC(
    tools = c(
        "fastqc",
        "trimmomatic",
        "sortmerna",
        "tophat",
        "hisat2",
        "samtools",
        "featureCounts"
    ),
    config = multiqc_config
)

# mapping stats ################################################################

if (!exists("number_trimmed")) {
  warning("Variable 'number_trimmed' undefined. Setting it to NA.")
  number_trimmed <- NA
}
if (!exists("number_nonrRNA")) {
  warning("Variable 'number_nonrRNA' undefined. Setting it to NA.")
  number_nonrRNA <- NA
}

# precise mapping stats requires BAM sorting.
# If TRUE, Bioconductor functions are used to determine mapping stats.
precise <- FALSE

mapping_stats_df <- calc_dual_mapping_stats(
    number_reads    = number_raw,
    raw_fastq_files = fastq_files,
    name_sp1        = name_sp1,
    name_sp2        = name_sp2,
    genome_sp1      = genome_sp1,
    genome_sp2      = genome_sp2,
    anno_sp1        = anno_sp1,
    anno_sp2        = anno_sp2,
    counts_sp1      = counts_sp1,
    counts_sp2      = counts_sp2,
    bam_files       = bam_files,
    paired          = paired,
    cpus            = MAX_CPUS,
    samples         = samples
)

# just a logical test
if (paired) {
    if (F %in% (asPaired(number_raw) >= lib_sizes))
        stop("Error: number of reads mapping in exons should not exceed original number of reads!")
} else {
    if (F %in% (number_raw >= lib_sizes))
      stop("Error: number of reads mapping in exons should not exceed original number of reads!")
}

# sort mapping stats by rownames in a more natural way
library("gtools")

# write mapping stats to CSV file
write_count_table(
    file   = file.path(tabDir, "mapping_stats"),
    counts = mapping_stats_df[mixedsort(rownames(mapping_stats_df)),],
    rnames = TRUE
)

# write mapping stats to XLS file
write_count_table(
    file       = file.path(tabDir, "mapping_stats"),
    counts     = mapping_stats_df[mixedsort(rownames(mapping_stats_df)),],
    as.xls     = TRUE,
    sheetNames = basename(getwd()),
    rnames     = TRUE
)

# Normalized count values ######################################################

rpkm_sp1 <- get_rpkm(counts_sp1, gene_length_sp1, colSums(counts_sp1))
rpkm_sp2 <- get_rpkm(counts_sp2, gene_length_sp2, colSums(counts_sp2))
tpm_sp1  <- get_tpm( counts_sp1, gene_length_sp1, colSums(counts_sp1))
tpm_sp2  <- get_tpm( counts_sp2, gene_length_sp2, colSums(counts_sp2))
mrn_sp1  <- get_mrn(counts_sp1)
mrn_sp2  <- get_mrn(counts_sp2)

# write values to CSV file
write_count_table(
    file   = file.path(tabDir, paste0(name_sp1, "_rpkm")),
    counts = rpkm_sp1
)
write_count_table(
    file   = file.path(tabDir, paste0(name_sp2, "_rpkm")),
    counts = rpkm_sp2
)
write_count_table(
    file   = file.path(tabDir, paste0(name_sp1, "_tpm")),
    counts = tpm_sp1
)
write_count_table(
    file   = file.path(tabDir, paste0(name_sp2, "_tpm")),
    counts = tpm_sp2
)
write_count_table(
    file   = file.path(tabDir, paste0(name_sp1, "_mrn")),
    counts = mrn_sp1
)
write_count_table(
    file   = file.path(tabDir, paste0(name_sp2, "_mrn")),
    counts = mrn_sp2
)

# write values to XLS file
# slow for huge tables, e.g. human genome and many conditions
write_count_table(
    file       = file.path(tabDir, paste0(name_sp1, "_rpkm")),
    counts     = rpkm_sp1,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp2, "_rpkm")),
    counts     = rpkm_sp2,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp1, "_tpm")),
    counts     = tpm_sp1,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp2, "_tpm")),
    counts     = tpm_sp2,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp1, "_mrn")),
    counts     = mrn_sp1,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)
write_count_table(
    file       = file.path(tabDir, paste0(name_sp2, "_mrn")),
    counts     = mrn_sp2,
    as.xls     = TRUE,
    sheetNames = basename(getwd())
)

# clustering ###################################################################

# NOTE: experimental function. Check resulting vector!
conds_sp1 <- conditions_from_design(design_matrix_sp1)
conds_sp2 <- conditions_from_design(design_matrix_sp2)

# hierarchical clustering heat map version
make_heat_clustering_plot(
    file.path(plotDir, paste0(name_sp1, "_heat_hierarchical_clustering")),
    counts    = counts_sp1[, conds_sp1 != "none"],
    conds     = conds_sp1[conds_sp1 != "none"]
)
make_heat_clustering_plot(
    file.path(plotDir, paste0(name_sp2, "_heat_hierarchical_clustering")),
    counts    = counts_sp2[, conds_sp2 != "none"],
    conds     = conds_sp2[conds_sp2 != "none"]
)

# hierarchical clustering
make_hclust_plot(
    file.path(plotDir, paste0(name_sp1, "_hierarchical_clustering")),
    counts = counts_sp1[, conds_sp1 != "none"],
    conds  = conds_sp1[conds_sp1 != "none"]
)

make_hclust_plot(
    file.path(plotDir, paste0(name_sp2, "_hierarchical_clustering")),
    counts = counts_sp2[, conds_sp2 != "none"],
    conds  = conds_sp2[conds_sp2 != "none"]
)

# PEARSON CORRELATION ##########################################################

# NOTE: Regression is performed. This takes a considerable amount of time
#       for large gene sets and/or samples.
make_correlation_plots(
    dat          = mrn_sp1,
    outDir       = plotDir,
    prefix       = paste0(name_sp1, "_corr_", ),
    designMatrix = design_matrix_sp1
)
make_correlation_plots(
    dat          = mrn_sp2,
    outDir       = plotDir,
    prefix       = paste0(name_sp2, "_corr_"),
    designMatrix = design_matrix_sp2
)

# PCA plots ####################################################################

# NOTE: if designMatrix is supplied, 'conds' is ignored. In that case, use 'designMatrix = NA'
conds_sp1 <- conditions_from_design(design_matrix_sp1)
conds_sp2 <- conditions_from_design(design_matrix_sp2)
colName <- "conditions"   # <-- fill in!
## in this vector, time series information can be supplied.
shapes_sp1 <- NULL        # <-- optional. fill in!
shapes_sp2 <- NULL        # <-- optional. fill in!
shapeName <- "replicates" # <-- optional. fill in!
# Fix conds
#conds_sp1 <- gsub("_.+$", "", conds_sp1)
#conds_sp2 <- gsub("_.+$", "", conds_sp2)
make_PCA_plot(
    file         = file.path(plotDir, paste0(name_sp1, "_pca_")),
    counts       = counts_sp1[, conds_sp1 != "none"],
    designMatrix = NA,
    conds        = conds_sp1[conds_sp1 != "none"],
    shapes       = shapes_sp1[conds_sp1 != "none"],
    norm         = "auto",
    main         = paste0("PCA - ", name_sp1),
    colName      = colName,
    shapeName    = shapeName
)
make_PCA_plot(
    file         = file.path(plotDir, paste0(name_sp2, "_pca_"),
    counts       = counts_sp2[, conds_sp2 != "none"],
    designMatrix = NA,
    conds        = conds_sp2[conds_sp2 != "none"],
    shapes       = shapes_sp2[conds_sp2 != "none"],
    norm         = "auto",
    main         = paste0("PCA - ", name_sp2),
    colName      = colName,
    shapeName    = shapeName
)

# differential expressed genes #################################################

pvalcut <- 0.01
logfcCut <- 1
tools <- c("DESeq", "DESeq2", "edgeR", "limma")
colnames(counts) <- samples
deg_anno <- "" # <-- optional: fill in if you need additional annotation in DEG tables

# dual RNA-seq experiments (should) have two different design matrices
# TODO this is a quick'n'dirty solution --> how to improve?
design_matrix_sp1 <- parse_SDRF("meta_data_sp1.csv", skip = 1, col.name.row = 1)$designMatrix
design_matrix_sp2 <- design_matrix

# NOTE: this will be a more complex structure in the future
deg_res_sp1 <- calculate_DEGs(
    counts       = counts_sp1,
    geneLengths  = gene_length_sp1,
    libSizes     = colSums(counts_sp1),
    designMatrix = design_matrix,
    pValCut      = pvalcut,
    logfcCut      = logfcCut,
    tools        = tools,
    outDir       = file.path(degDir, name_sp1),
    prefix       = paste0(name_sp1, "_"),
    anno         = deg_anno,
    stop.on.error = FALSE,
    cpus          = MAX_CPUS,
    workers        = 10
)

deg_res_sp2 <- calculate_DEGs(
    counts       = counts_sp2,
    geneLengths  = gene_length_sp2,
    libSizes     = colSums(counts_sp2),
    designMatrix = design_matrix,
    pValCut      = pvalcut,
    logfcCut      = logfcCut,
    tools        = tools,
    outDir       = file.path(degDir, name_sp2),
    prefix       = paste0(name_sp2, "_"),
    anno         = deg_anno,
    stop.on.error = FALSE,
    cpus          = MAX_CPUS,
    workers        = 10
)

# update SDRF metadata
# it's not a setting for the DEG tools directly,
# hence writing pvalcut and logfcCut to Description and not Parameters.And.Values
software_nr <- software_nr + 1
col <- paste0("Description.", software_nr)
SDRF[, col] <- deg_res$desc
col <- paste0("Data.Processing.Software.", software_nr)
SDRF[, col] <- paste(deg_res$tools, collapse = ", ")
col <- paste0("Storage.Date.", 3)
SDRF[, col] <- Sys.Date()

deg_overview_sp1 <- make_deg_overview_plot(
    outDir = degDir,
    degs = deg_res_sp1$DEGs,
    tools = tools
)

deg_overview_sp2 <- make_deg_overview_plot(
    outDir = degDir,
    degs = deg_res_sp2$DEGs,
    tools = tools
)

# Number of DEGs per comparison, where all used tools agree:
print(deg_overview_sp1)
print(deg_overview_sp2)

# fill in! <-- Archive directory of the final results
# SDRF$Storage.Location.3 <- "stored at HKI Backupserver" # old SDRF version
SDRF$Storage.Location...Server.1 <- "stored at HKI Jena: /sbiarchiv" # according to FungiNet Lookup-Table
SDRF$Storage.Location...Folder.1 <- "/sbiarchiv/xxx"
SDRF$Derived.Data.File <- "results.zip"

# save results #################################################################

# save all R objects
save.image("R_workspace.RData")

# TODO dual: save to two SDRF files???
# --> if yes: fill second SDRF right from the beginning?

# save updated (filled) SDRF table
write.table(
    SDRF,
    file      = paste0(tools::file_path_sans_ext(SDRF_file), "_SDRF_final.csv"),
    sep       = "\t",
    quote     = FALSE,
    row.names = FALSE
)

# zip important result files, e.g. for upload to FungiNetDB
archive(zipFile = "results.zip")

# optional: cleanup
system(paste("rm", "-r", fastqDir, qualDir, mapDir, countDir))

# optional: move remaining files to archive directory
system("mv * xxx") # <-- fill in!
