#####################################################################
# Code snippet to retrieve additional gene attributes from biomaRt. #
#####################################################################

source("http://bioconductor.org/biocLite.R")

biocLite("biomaRt")
library(biomaRt)

ensembl <- useEnsembl(
    biomart = "ensembl",
    dataset = "hsapiens_gene_ensembl",
    version = "89"
)
genes <- read.table("genes") # IDs present in output table from pipeline
# or genes <- rownames(DEGs)

tok <- getBM(
    attributes = c(
        'ensembl_gene_id',
        'external_gene_name',
        'description',
        'chromosome_name',
        'gene_biotype'
    ),
   filters = 'ensembl_gene_id',
   values = genes,
   mart = ensembl
)

write.table(tok, file = "gene_names.tsv", sep = "\t", row.names = F)
