#!/bin/bash

## Informations
#  title            : getMeta.sh
#  description      : This script creates a metadata table for available
#                     RNA-seq data in GEO (Gene Expression Omnibus) for
#                     a given organism
#  author           : Silvia Mueller
#  date             : 2015/05/19
#  usage            : bash getMeta.sh "Organism" /full/Path/to/storage/directory
#  usage example    : bash getMeta.sh "Candida albicans" /home/silvia/RNA-Seq/

# Beispiel Suchanfrage für NCBI
# "Aspergillus fumigatus"[Organism] AND "Expression profiling by high throughput sequencing"[Filter]

## keep temp output for debugging?
debug=0

## Date of today
#  today: contains date of today in YYYY/MM/DD
today=$(date +"%Y/%m/%d")

## log file
log="$(pwd)/getMeta.log"
echo $today > $log

## Test if there is the right number of arguments supplied
if [ $# -eq 0 ]
then
    ## Output that there is not enough arguments supplied
    echo "no arguments supplied"
    ## Output how to use it
    echo "Usage: ./getMeta.sh \"Organism\" /full/path/to/storage/directory"
elif [ $# -eq 1 ]
then
    echo "not enough arguments supplied"
    echo "Usage: ./getMeta.sh \"Organism\" /full/path/to/storage/directory"
## Test if there is the right number of arguments supplied
elif [ $# -eq 2 ]
then
    outputpath=$2
    cd $outputpath
    ## Starttime for runtime
    #  before: contains current system time
    before=$(date +%s)

    ## Transformations of the input
    #  org_param: input organism in all lower case
    org_param=$1
    org_param=$(echo $org_param | tr '[:upper:]' '[:lower:]')
    # org_first: input organism, first part, first letter upper case (genus)
    org_first=$(echo $org_param | awk '{ print $1 }')
    org_first="$(tr '[:lower:]' '[:upper:]' <<< ${org_param:0:1})${org_first:1}"
    # org_second: input organism, second part, all lower case (species)
    org_second=$(echo $org_param | awk '{ print $2 }')
    # organism: input organism with correct upper and lower cases
    organism="$org_first $org_second"
    # orgfolder: organism foldername with underscore between
    orgfolder=$(echo $organism | tr " " "_" )
    # orgn: organism transformed for search (all lower case, + between)
    orgn=$(echo $organism | tr [:upper:] [:lower:] | sed 's/ /+/g')

    ## Basic URLs
    # basicURLeUtils: basic URL for using the eutils
    basicURLeUtils="eutils.ncbi.nlm.nih.gov/entrez/eutils/"
    # basicURLGEO: basic URL for query in GEO
    basicURLGEO="ncbi.nlm.nih.gov/geo/query"

    ## Creating organism folder and enter
    mkdir "$orgfolder" 2>/dev/null
    cd "$orgfolder"

    ## Filter for RNA-seq data
    #  filter: just RNA-seq filter as used in GEO
    filter='("Expression profiling by high throughput sequencing"[Filter])'

    ## Building the searchterm
    #  searchterm: consists of
    #  $orgn as organism
    #  $filter as filter
    #  retmax as max limit of retrieved entries
    #  datetype for date
    #  $olddate is the date of the last search, if there was any
    #  $today as the end of the timerange for search, if there was a search before
    #  Test if this search was run before
    #  if it had been run before, there is an invisible help file in the organism directory
    if [ -f .helpdate.txt ]
    then
        # if it was run before, extract the date out of the help file
        olddate=$(cat .helpdate.txt | tr -d " " | tr -d '\r' | tr -d '\n')
        # the searchterm is appended by the date of the old search
        # so just new data since last search is retrieved
        searchterm=$orgn"[organism]+AND+"$filter"&retmax=10000&datetype=pdat&mindate="$olddate"&maxdate="$today
    else
        # if there was no run before, search without date limit
        searchterm=$orgn"[organism]+AND+"$filter"&retmax=10000"
    fi

    ## Building URL for the search of IDs
    searchForIDs=$basicURLeUtils"esearch.fcgi?db=gds&term="$searchterm

    ## Getting IDs
    #  listOfIDs: list with the IDs in GEO
    RET=1
    until [ "$RET" -eq 0 ]
    do
        listOfIDs=$(wget -q "$searchForIDs" -O - | grep "<Id>" | rev | cut -c6- | rev | sed 's/<Id>//g')
        RET=$?
        if [ "$RET" -ne 0 ]
        then
            sleep 10
        fi
    done

    ## helping counter
    #  count_gsm: used to deleting empty folders
    count_gsm=0

    ## Creating temp folder and enter it
    mkdir "temp" 2>/dev/null
    cd "temp"

    ## Getting XML file with summary for every ID
    #  for every ID in the listOfIDs
    for ID in $listOfIDs
    do
        # searchSummary: URL for using eSummary of the eUtils to get the
        #                XML file for the ID
        # this XML file contains informations about the GSE numbers
        # associated with this ID
        searchSummary=$basicURLeUtils"esummary.fcgi?db=gds&id="$ID"&retmax=10000"
        # GSE: GSE accession number
        # GSE accession number is retrieved out of the ID XML file
        RET=1
        until [ "$RET" -eq 0 ]
        do
            GSE=$(wget -q "$searchSummary" -O - | grep "GSE" | head -n 1 | awk -F"<Item Name=" '{ print $2 }' | awk -F"</Item>" '{ print $1 }' | awk -F">" '{ print $2 }' )
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        echo "working on $GSE:"

        ## create GSE directory
        mkdir $GSE 2>/dev/null

        ## Create list of GSM accessions, which belong to the GSE Experiment
        #  test if there was an error in wget
        RET=1
        until [ "$RET" -eq 0 ]
        do
            #  listOfGSM: list of GSM accessions
            listOfGSM=$(wget -q "$searchSummary" -O - | grep "GSM" | awk -F"<Item Name=" '{ print $2 }' | awk -F"</Item>" '{ print $1 }' | awk -F">" '{ print $2 }' )
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        ## enter GSE directory
        cd $GSE

        ## create name of the output file for GSE information
        outputGSE=$GSE".txt"

        ## Create URL for retrieving the XML file for GSE accession
        url_GSE=$basicURLGEO"/acc.cgi?acc="$GSE"&form=text"

        ## Getting the XML file for GSE accession
        RET=1
        until [ "$RET" -eq 0 ]
        do
            #  listOfGSM: list of GSM accessions
            wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputGSE "$url_GSE"
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        ## Helping counter
        #  dircount: helping counter for deleting empty folders
        dircount=0

        ## Getting GSM information files
        #  for every GSM accession in list
        for GSM_acc in $listOfGSM
        do
            # URL for retrieving the GSM information file
            url_gsm=$basicURLGEO"/acc.cgi?targ=gsm&acc="$GSM_acc"&form=text"

            # c1 is a helping variable, which contains the sample organisms in GSM information file
            RET=1
            until [ "$RET" -eq 0 ]
            do
                c1=$(wget -q "$url_gsm" -O - | grep "!Sample_organism" | wc -l)
                RET=$?
                if [ "$RET" -ne 0 ]
                then

                    sleep 10
                fi
            done
            # test if there is more than one organism (e.g. candida + human)
            if [[ $c1 == 1 ]]
            then
                # c3 is a helping variable, which contains the the sample organism
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    c3=$(wget -q "$url_gsm" -O - | grep "!Sample_organism" | cut -c24- | tr -d '\r')
                    c3=$(echo $c3 | awk '{print $1" "$2}')
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # c2 is a helping variable, which contains the Library Strategy
                # c2 is used to test if it is RNA-seq (sometimes also ChipSeq)
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    c2=$(wget -q "$url_gsm" -O - | grep "!Sample_library_strategy" | cut -c28- | tr -d '\r')
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done
            elif [[ $c1 == 2 ]]
            then
                c3="Dual-Seq"
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    c2=$(wget -q "$url_gsm" -O - | grep "!Sample_library_strategy" | cut -c28- | tr -d "\t")
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done
            else
                # if there is more than two organism, set helping variables to fail next test
                c3=""
                c2=""
            fi

            # test if there is only one organism OR two organism (dual seq) AND
            # test if it is the right organism AND
            # test if it is really RNA-seq
            if ([[ $c1 == 1 ]] || [[ $c1 == 2 ]]) && ([[ $c3 == $organism ]] || [[ $c3 == *Dual-Seq* ]]) && [[ $c2 == *RNA* ]]
            then
                # if test passed
                echo "$GSM_acc"

                # create GSM directory and enter it
                mkdir "$GSM_acc" 2>/dev/null
                cd "$GSM_acc"

                # outputTXTGSM: name of the output file for GSM information
                outputTXTGSM=$GSM_acc".txt"

                # getting GSM information file
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputTXTGSM "$url_gsm"
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # extract strain to strain list
                # the variable stamm is used here for building this list
                # characteristics: extracted characteristics of GSM information file
                characteristics=$(grep "!Sample_characteristics_ch1" < $outputTXTGSM | cut -c31- | tr '\r' ";")

                # test if characteristics contains the word "strain"
                if [[ $c1 == 1 ]]
                then
                    if [[ $characteristics == *strain* ]]
                    then
                        # if characteristics contain the word "strain"
                        # c is a helping variable for counting the number of occurences of the word "strain" in characteristics
                        c=$(echo $characteristics | grep -oE "strain" | wc -l)
                        # s1 is a helping variable
                        s1=$organism" strain:"
                        # s2 is a helping variable
                        s2=$organism" strain background:"
                        # r is a regex for forms of "strain" with brackets before and after the word
                        r=".*\(.*strain\).*"
                        # test if there is just one occurence of the word "strain" in characteristics
                        if [[ $c -eq 1 ]]
                        then
                            # if there is just one occurence of the word "strain"
                            # test if the regex pattern is in characteristics
                            if echo $characteristics | grep -Eq "$r"
                            then
                                # if the regex pattern is in characteristics
                                # st is a helping variable, containing everything with the "strain" together in brackets
                                st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                                # st1 is a helping variable, containing the first word in brackets of st
                                st1=$(echo $st | awk '{ print $1 }')
                                # test if st1 contains the word "background"
                                if [[ "$st1" == *background* ]]
                                then
                                    # if st1 contains the word "background"
                                    # stamm is associated with a string, suggesting you have to look into characteristics, because there is no distinct strain named
                                    stamm="look into characteristics"
                                else
                                    # if st1 does not contain the word "background"
                                    # stamm is this strain
                                    stamm=$(echo $st1)
                                fi
                            # test if the pattern s1 is in characteristics
                            elif [[ $characteristics == *$s1* ]]
                            then
                                # if pattern s1 is in characteristics
                                # stamm is the word after pattern s1 until next ";"
                                stamm=$(echo $characteristics | awk -v s1="$s1" -F"$s1 " '{ print $2 }' | awk -F";" '{ print $1 }')
                            # test if the pattern s2 is in characteristics
                            elif [[ $characteristics == *$s2* ]]
                            then
                                # if pattern s2 is in characteristics
                                # stamm is the word after pattern s2 until next ";"
                                stamm=$(echo $characteristics | awk -v s2="$s2" -F"$s2 " '{ print $2 }' | awk -F";" '{ print $1 }')
                            else
                                # if no pattern is in characteristics
                                # stamm is the word after "strain: " until the next ";"
                                stamm=$(echo $characteristics | awk -F"strain: " '{ print $2 }' | awk -F";" '{ print $1 }')

                                # if stamm is still empty but characteristics contains strain/background
                                if [[ $stamm == "" && $characteristics == *"strain/background: "* ]]
                                then
                                    # stamm: contains everything after the string "strain/background :" until the next ";"
                                    stamm=$(echo $characteristics | awk -F"strain/background: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                                fi
                            fi
                        elif [[ $c -eq 2 ]]
                        then
                            if [[ $characteristics == *"strain: strain"* ]]
                            then
                                stamm=$(echo $characteristics | awk -F"strain: strain" '{ print $2 }' | awk -F";" '{ print $1 }')
                            fi
                        fi
                    else
                        # if there is no occurence of the word "strain" in characteristics
                        # stamm is not availabe
                        stamm="NA"
                    fi
                elif [[ $c1 == 2 ]]
                then
                    if [[ $characteristics == *"strain: strain" ]]
                    then
                        stamm=$(echo $characteristics | awk -F"strain: strain" '{print $2 }' | awk -F";" '{ print $1 }')
                    else
                        stamm="Dual-Seq"
                    fi
                fi

                # stamm is trimmed of whitespaces
                stamm=$(echo $stamm | tr -d " ")

                # strains: list of strains
                # strains is appended by stamm
                strains=$strains" "$stamm

                # srx_acc: SRX accession of the GSM information file
                # srx_acc=$(grep "sra-instant" < $outputTXTGSM | awk -F"/SRX/" '{ print $2 }' | awk -F"/" '{ print $2 }' | tr -d '\r')
                srx_acc=$(grep -oP 'sra\?term=\K\w+' $outputTXTGSM | tr -d '\r')

                # url_srxID: URL for retrieving SRX IDs
                url_srxID=$basicURLeUtils"esearch.fcgi?db=sra&term="$srx_acc"&retmax=10000"

                # srx_id: extracted SRX ID
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    srx_id=$(wget -q "$url_srxID" -O - | grep "<Id>" | rev | cut -c6- | rev | sed 's/<Id>//g')
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # get SRX metadata
                # url_srx: URL for retrieving SRX information file
                url_srx=$basicURLeUtils"efetch.fcgi?db=sra&id="$srx_id"&retmax=10000"
                # outputSRX: name of output file for SRX information
                outputSRX=$srx_acc".xml"
                # getting SRX information
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputSRX "$url_srx"
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # change to GSE directory
                cd ".."

                # increment help variable
                ((dircount++))
            fi
        done

        # increment help variable count_gsm (used to delete empty folders and output of number of GSM)
        count_gsm=$((count_gsm + $dircount))

        # change to temp directory
        cd ".."

        # remove directories without GSM
        # if help variable is null
        if [ $dircount == 0 ]
        then
            # remove the GSE folder (which is empty)
            rm -rf $GSE 2>/dev/null
        fi
    done

    # f_count: help variable to see if there is data available
    # if there is data: f_count is greater or equal 1
    f_count=$(ls -1 -p | grep "/" | wc -l)
    # test if help variable is greater or equal 1
    if [ $f_count -ge 1 ]
    then
        # change to organism directory
        cd ".."

        # afterdown: variable with current date
        # used for calculating running time
        # it is the time after retrieving the metadata
        afterdown=$(date +%s)

        # output of runtime for download of metafiles
        echo "Time for searching: " $((afterdown - $before)) "seconds"
        # output number of GSE founds
        echo "Number of GSE: " $f_count
        # output number of GSM founds
        echo "Number of GSM: " $count_gsm

        ##########################################
        ## Starting Building the Metadata table ##
        ##########################################

        # strains_uniq: eliminating double strains in list of strains
        strains_uniq=$(echo -e $strains | sed 's/ /\n/g' | sort -u)
        # for every strain s in the list of uniq strains
        for s in $strains_uniq
        do
            # if there is no folder with the name of the strain
            if ! [ -d "$s" ]
            then
                # create a folder with the name of the strain
                mkdir $s
            fi
        done

        # Build paths for output
        # directory: path to current directory
        directory=$(pwd)
        # now: current date with YearMonthDayHoursMinutes
        now=$(date +"%Y%m%d%H%M")
        # output: path to Metadata file with the new retrieved data
        output=$directory"/;;/"$orgfolder"_;;_Metadata_new_"$now".tsv"
        # all_tsv: path to metadata file with all retrieved data, which could be manualle sorted out
        all_tsv=$directory"/;;/"$orgfolder"_;;_Metadata_sorted.tsv"

        # get into temp folder
        cd "temp"

        # for every directory in temp folder (GSE folders)
        for i in $(ls -d */)
        do
            # GSE_dir: name of GSE folder
            GSE_dir=$(echo $i)
            # change to the GSE folder
            cd "$GSE_dir"

            # folder_count: number of folders
            folder_count=$(ls -1 -p | grep "/" | wc -l)

            # if there is at least one folder
            if [ $folder_count -ge 1 ]
            then
                # for every folder in GSE directory (GSM folders)
                for d in $(ls -d */);
                do
                    echo "-------------------------------------------------" >> $log

                    # GSM: name GSM folder and also the GSM accession number
                    GSM=$(echo $d|awk -F"/" '{print $1}')
                    # change to GSM folder
                    cd "$GSM"

                    # GSM_file: name of the GSM file
                    GSM_file=$GSM".txt"
                    echo "GSM file: $GSM_file" >> $log

                    # gse: GSE accession
                    gse=$(pwd | awk -F"/" '{ print $(NF-1) }')

                    # gse_file: GSE information file
                    gse_file="../$gse.txt"

                    # gse_title: Series Title in GSE file
                    gse_title=$(grep "!Series_title" < $gse_file | awk -F" = " '{ print $2 }' | tr "\r" ";" | tr "\n" ";" | tr -s ";")

                    # pubmed: Pubmed ID (sometimes NA)
                    # test if pubmed ID is available in GSE file
                    if grep -q "!Series_pubmed_id" < $gse_file
                    then
                        # if pubmed ID is availale, pubmed is assigned this ID
                        pubmed=$(grep "!Series_pubmed_id" < $gse_file | awk -F" = " '{ print $2 }' |tr "\r" ";"| tr "\n" ";"| tr -s ";")
                    else
                        # if no pubmed ID available, pubmed is NA
                        pubmed="NA"
                    fi

                    # gse_status: Series status in GSE file
                    gse_status=$(grep "!Series_status" < $gse_file | awk -F" = " '{ print $2 }' | tr "\r" ";"| tr "\n" ";"| tr -s ";")

                    # gse_lastupdate: Series last update in GSE file
                    gse_lastupdate=$(grep "!Series_last_update" < $gse_file | awk -F" = " '{ print $2 }'| tr "\r" ";"| tr "\n" ";"| tr -s ";")

                    # gse_summary: Series summary in GSE file
                    gse_summary=$(grep "!Series_summary" < $gse_file | awk -F" = " '{ print $2 }' |tr "\r" ";"| tr "\n" ";"| tr -s ";")

                    # gse_overall: Series overall design in GSE file
                    gse_overall=$(grep "!Series_overall_design" < $gse_file | awk -F" = " '{ print $2 }'| tr "\r" ";"| tr "\n" ";"| tr -s ";")

                    # gse_contact: Series Contact (seperated by ;) in GSE file
                    gse_contact=$(grep "!Series_contact_" < $gse_file | awk -F"Series_contact_" '{ print $2 }' | awk -F" = " '{ print $1" : "$2 }' | tr '\r' ";"| tr "\n" ";"| tr -s ";" | sed 's/;;/;/')

                    # gse_author: extracted name from gse_contact
                    gse_author=$(echo $gse_contact| awk -F"name : " '{ print $2 }' | awk -F";" '{ print $1 }' | sed 's/,/_/' | sed 's/,/_/' | sed 's/__/_/' | sed 's/_//')

                    # if there is the flag "Series Contributors" in GSE file
                    if grep -q "!Series_contributor" < $gse_file
                    then
                        # gse_contrib: Series contributors
                        gse_contrib=$(grep "!Series_contributor" < $gse_file | awk -F" = " '{ print $2 }' | awk -F",," '{ print $1" "$2 }' | tr '\r' ";"| tr "\n" ";"| tr -s ";")
                    else
                        # gse_contrib: not available
                        gse_contrib="NA"
                    fi

                    # specimen: Sample title from GSM file
                    # specimen=$(grep "!Sample_title" < $GSM_file | cut -c11- | tr -d '\r')
                    specimen=$(grep -oP '\!Sample_title = \K.*' $GSM_file | tr -d '\r')
                    echo "Specimen: $specimen" >> $log

                    # characteristics: Sample characteristics from GSM file
                    characteristics=$(grep "!Sample_characteristics_ch1" < $GSM_file | cut -c31- | tr '\r' ";"| tr "\n" ";"| tr -s ";")
                    echo "Characteristics: $characteristics" >> $log

                    c1=$(grep "!Sample_organism" < $GSM_file | wc -l)

                    if [[ $c1 == 1 ]]
                    then
                        # test if characteristics contains strain information
                        if [[ $characteristics == *strain* ]]
                        then
                            # c is a help variable counting the number of occurences of the word "strain" in the characteristics variable
                            c=$(echo $characteristics | grep -oE "strain" | wc -l)

                            # s1 is a help variable as pattern for example "Candida albicans strain:"
                            s1=$organism" strain:"
                            # s2 is a help variable as pattern for example "Candida albicans strain background:"
                            s2=$organism" strain background:"
                            # r is a regex pattern for strain related information in brackets, like "(SC5314 strain)"
                            r=".*\(.*strain\).*"
                            # test if there is just one occurence of word "strain"
                            if [[ $c -eq 1 ]]
                            then
                                # test if regex pattern r is in characteristics like "(background strain)" or "(SC5314 strain)"
                                if echo $characteristics | grep -Eq "$r"
                                then
                                    # st is a help variable containing the words between brackets eg "background strain" or "SC5314 strain"
                                    st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                                    # st1 is a help variable containing the first word in brackets eg "background" or "SC5314"
                                    st1=$(echo $st | awk '{ print $1 }')
                                    # test if st1 contains the word "background"
                                    if [[ "$st1" == *background* ]]
                                    then
                                        # if it contains "background" there is probabily some information in characteristics which can be retrieved manually
                                        strain="look into characteristics"
                                    else
                                        # if it does not contains "background" the first word is most likely the name of the strain
                                        strain=$(echo $st1)
                                    fi
                                else
                                    # if the regex pattern r is not found
                                    # strain: contains everything after the string "strain :" until the next ";"
                                    strain=$(echo $characteristics | awk -F"strain: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")

                                    # if strain is still empty but characteristics contains strain/background
                                    if [[ $strain == "" && $characteristics == *"strain/background: "* ]]
                                    then
                                        # strain: contains everything after the string "strain/background :" until the next ";"
                                        strain=$(echo $characteristics | awk -F"strain/background: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                                    fi
                                fi
                            else
                                # if there are more occurences of the word "strain"
                                # s1 is a help variable as pattern for example "Candida albicans strain:"
                                s1=$organism" strain:"
                                # test if characteristics contains the string s1
                                if [[ $characteristics == *$s1* ]]
                                then
                                    # strain: everything after string s1 and before next ";"
                                    strain=$(echo $characteristics | awk -v s1="$s1" -F"$s1 " '{ print $2 }' | awk -F";" '{ print $1 }'| tr -d " ")
                                else
                                    # s2 is a help variable as pattern for example "Candida albicans strain background:"
                                    s2=$orga" strain background:"
                                    # test if characteristics contains the string s2
                                    if [[ $characteristics == *$s2* ]]
                                    then
                                        # strain: everything after string s2 and before next ";"
                                        strain=$(echo $characteristics | awk -v s2="$s2" -F"$s2 " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                                    else
                                        # strain: can manually be retrieved
                                        strain="look into characteristics"
                                    fi
                                fi
                                if [[ $characteristics == *"strain: strain"* ]]
                                then
                                    strain=$(echo $characteristics | awk -F"strain: strain" '{ print $2 }' | awk -F";" '{ print $1 }')
                                fi
                            fi
                        # test if characteristics contain the string "strain)"
                        elif [[ $characteristics == *"strain)"* ]]
                        then
                            # st is a help variable containing everything between the brackets
                            st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                            # st1 is a help variable containing the first word in the brackets
                            st1=$(echo $st | awk '{ print $1 }')
                            # test if st1 contains the string "background"
                            if [[ "$st1" == *background* ]]
                            then
                                # strain: strain information can possibly manually retrieved
                                strain="look into characteristics"
                            else
                                # strain: st1 is most likely the name of the strain
                                strain=$(echo $st1 | tr -d " ")
                            fi
                        else
                            # strain: not available
                            strain="NA"
                        fi
                    elif [[ $c1 == 2 ]]
                    then
                        if [[ $characteristics == *"strain: strain" ]]
                        then
                            stamm=$(echo $characteristics | awk -F"strain: strain" '{print $2 }' | awk -F";" '{ print $1 }')
                        else
                            stamm="Dual-Seq"
                        fi
                    fi
                    # strain_f: trimming white spaces out of strain
                    strain_f=$(echo $strain | tr -d " ")
                    echo "Strain: $strain_f" >> $log

                    # org: Sample organism ch1 in GSM file
                    org=$(grep "!Sample_organism" < $GSM_file | cut -c24- | tr '\r' ";")
                    echo "Organism: $org" >> $log

                    # taxid: sample taxid ch1 in GSM file
                    taxid=$(grep "!Sample_taxid_ch1" < $GSM_file | cut -c21- | tr '\r' ";")
                    echo "Taxon ID: $taxid" >> $log

                    # test if "!Sample_treatment_protocol" information can be found in GSM file
                    if grep -q "!Sample_treatment_protocol" < $GSM_file
                    then
                        # if there is information available
                        # treat: containing treatment protocol from GSM file
                        treat=$(grep "!Sample_treatment_protocol" < $GSM_file | awk -F " = " '{ print $2 }' | tr '\r' ";"| tr "\n" ";")
                    else
                        # if there is no information available
                        # treat: not available
                        treat="NA"
                    fi

                    echo "Treatment: $treat" >> $log

                    # test if "!Sample_growth_protocol" information can be found in GSM file
                    if grep -q "!Sample_growth_protocol" < $GSM_file
                    then
                        # if there is information available
                        # growth: containing growth protocol from GSM file
                        growth=$(grep "!Sample_growth_protocol_ch1" < $GSM_file | cut -c31- | tr '\r' ";"| tr "\n" ";")
                    else
                        # if there is no information available
                        # growth: not available
                        growth="NA"
                    fi

                    echo "Growth protocol: $growth" >> $log

                    # mt: Material Type (total RNA, polyA RNA ...) from GSM file
                    mt=$(grep "!Sample_molecule_ch1" < $GSM_file | cut -c24- | tr -d '\r')
                    echo "Material type: $mt" >> $log

                    # extraction: Extraction protocol from GSM file
                    extraction=$(grep "!Sample_extract_protocol_ch1" < $GSM_file | cut -c32- | tr '\r' ";"| tr "\n" ";")
                    echo "Extraction protocol: $extraction" >> $log

                    # instrument: sequencing instrument model from GSM file
                    instrument=$(grep "!Sample_instrument_model" < $GSM_file | cut -c28- | tr -d '\r')
                    echo "Sequencing instrument: $instrument" >> $log

                    # libsource: Library Source from GSM file (mostly transcriptomic)
                    libsource=$(grep "!Sample_library_source" < $GSM_file | cut -c26- | tr -d '\r')
                    echo "Library source: $libsource" >> $log

                    # libselection: Library selection from GSM file (like cDNA)
                    libselection=$(grep "!Sample_library_selection" < $GSM_file | cut -c29- | tr -d '\r')
                    echo "Library selection: $libselection" >> $log

                    # srx_acc: extracted SRX accession from GSM file
                    # srx_acc=$(grep "sra-instant" < $GSM_file | awk -F"/SRX/" '{ print $2 }' | awk -F"/" '{ print $2 }' | tr -d '\r')

                    # example line in GSM file: "!Sample_relation = SRA: https://www.ncbi.nlm.nih.gov/sra?term=SRX403442"
                    srx_acc=$(grep -oP 'sra\?term=\K\w+' $GSM_file | tr -d '\r')
                    echo "SRX: $srx_acc" >> $log

                    # srxfile: name of information file for SRX Accession
                    srxfile=$srx_acc".xml"
                    # liblayout: here just extracted information between flags in information file from SRX accession
                    liblayout=$(grep "LIBRARY_LAYOUT" < $srxfile | awk -F"<LIBRARY_LAYOUT>" '{ print $2 }' | awk -F"</LIBRARY_LAYOUT>" '{ print $1 }' | tr -cd '[:alpha:]')

                    # formatting output of liblayout
                    # if liblayout contains the string "PAIRED"
                    if [[ $liblayout == *PAIRED* ]]
                    then
                        # liblayout: is paired end
                        liblayout="paired end"
                    # if liblayout contains the string "SINGLE"
                    elif [[ $liblayout == *SINGLE* ]]
                    then
                        # liblayout: is single end
                        liblayout="single end"
                    fi

                    echo "Library layout: $liblayout" >> $log

                    # seq: extracted library strategy from GSM file ( mostly RNA Seq )
                    # attention: library strategy in GEO is something totally different from library strategy in FungiNet
                    seq=$(grep "!Sample_library_strategy" < $GSM_file | cut -c28- | tr -d '\r')
                    echo "Library strategy: $seq" >> $log

                    # Library Strategy
                    # polyregex: REGEX pattern for recognition whether just mRNA was extracted
                    # polyregex matches the following
                    # mRNA, polyadenylated, Polyadenylated, poly-adenylated, poly-A, poly-a, Poly-A, Poly-a, Poly A, Poly a,
                    # poly A, poly a, Ribo-Minus, Ribo Minus, RiboMinus, Ribominus, Ribo-minus, Ribo minus, RiboZero, Ribozero,
                    # Ribo Zero, Ribo zero, Ribo-Zero, Ribo-zero, Poly(A), poly(A), poly(a), Poly(a), Poly (A), Poly (a),
                    # poly (A), poly (a), Poly-(A), Poly-(a), poly-(A), poly-(a), PolyA, polyA, polya, Polya
                    polyregex=".*(mRNA|[pP]oly[- ]?(?:adenylated|[Aa]\s+|\([Aa]\))\s+|Ribo[- ]?[mM]inus|Ribo[- ]?[zZ]ero).*"
                    # test if the regex can be found in extraction information
                    if echo $extraction | grep -Eq "$polyregex"
                    then
                        # if the regex can be found in extraction information
                        # polyA: is positiv
                        polyA="yes"
                    # test if the regex can be found in material type information
                    elif echo $mt | grep -Eq "$polyregex"
                    then
                        # if the regex can be found in material type information
                        # polyA: is positiv
                        polyA="yes"
                    # test if the regex can be found in characteristics
                    elif echo $characteristics | grep -Eq "$polyregex"
                    then
                        # if the regex can be found in characteristics
                        # polyA: is positiv
                        polyA="yes"
                    else
                        # if the regex can not be found anywhere
                        # polyA: information is not available
                        polyA="NA"
                    fi

                    # strandregex: regex for different notations of strand-specific
                    strandregex=".*([sS]trand[- ]?specific).*"
                    # nonstrand: regex for different notations of non-strand-specific
                    nonstrand=".*(non[- ]?[sS]trand[- ]?specific).*"
                    # nonstrandregex: regex for different notations of strand-non-specific
                    nonstrandregex=".*([sS]trand[- ]?non[- ]?specific).*"
                    # test if extraction contains strandregex AND nonstrand
                    if (echo $extraction | grep -Eq "$strandregex") && (echo $extraction | grep -Eqv "$nonstrand")
                    then
                        # test if extraction contains nonstrandregex (because there could be conflicting informations in freetext)
                        if echo $extraction | grep -Eq "$nonstrandregex"
                        then
                            # strand: double (means, that there is a double match and resulting conflicting informations)
                            strand="double"
                        # test if characteristics contains nonstrandregex (because there could be conflicting informations in freetext)
                        elif echo $characteristics | grep -Eq "$nonstrandregex"
                        then
                            # strand: double (means, that there is a double match and resulting conflicting informations)
                            strand="double"
                        else
                            # if nonstrandregex can not be found, there is most likely no confliciting informations
                            # strand: yes (it is strand specific)
                            strand="yes"
                        fi
                    # test if characteristics contains strandreged AND nonstrand
                    elif (echo $characteristics | grep -Eq "$strandregex") && (echo $characteristics | grep -Eqv "$nonstrand")
                    then
                        # test if extraction contains nonstrandregex (because there could be conflicting informations in freetext)
                        if echo $extraction | grep -Eq "$nonstrandregex"
                        then
                            # strand: double (means, there is a double match and resulting conflicting informations)
                            strand="double"
                        # test if characteristics contains nonstrandregex (because there could be conflicting informations in freetext)
                        elif echo $characteristics | grep -Eq "$nonstrandregex"
                        then
                            # strand: double (means, there is a double match and resulting conflicting informations)
                            strand="double"
                        else
                            # strand: yes (it is strand specific)
                            strand="yes"
                        fi
                    # test if extraction contains nonstrandregex
                    elif echo $extraction | grep -Eq "$nonstrandregex"
                    then
                        # strand: no (it is non-strand-specific)
                        strand="no"
                    # test if characteristics contains nonstrandregex
                    elif echo $characteristics | grep -Eq "$nonstrandregex"
                    then
                        # strand: no (it is non-strand-specific)
                        strand="no"
                    else
                        # if nothing matchs
                        # strand: not available
                        strand="NA"
                    fi

                    # test if polyA is yes
                    if [[ "$polyA" == "yes" ]]
                    then
                        # test if strand is yes
                        if [[ "$strand" == "yes" ]]
                        then
                            # libstrategy: liblayaout (single/paired end); poly(A), strand-specific
                            libstrategy=$liblayout"; poly(A); strand-specific"
                        # test if strand is no
                        elif [[ "$strand" == "no" ]]
                        then
                            # libstrategy: liblayout (single/paired end); poly(A), strand-non-specific
                            libstrategy=$liblayout"; poly(A); strand-non-specific"
                        # test if strand is double
                        elif [[ "$strand" == "double" ]]
                        then
                            # libstrategy: liblayout (single/paired end); poly(A), manual information for strand
                            libstrategy=$liblayout"; poly(A); look into characteristics and/or extraction for strand information"
                        else
                            # no information about strand
                            # libstrategy: liblayout (single/paired end); poly(A)
                            libstrategy=$liblayout"; poly(A)"
                        fi
                    # test if polyA is NA
                    elif [[ "$polyA" == "NA" ]]
                    then
                        # test if strand is yes
                        if [[ "$strand" == "yes" ]]
                        then
                            # libstrategy: liblayout (single/paired end); strand specific
                            libstrategy=$liblayout"; strand-specific"
                        # test if strand is no
                        elif [[ "$strand" == "no" ]]
                        then
                            # libstrategy: liblayout (single/paired end); strand-non-specific
                            libstrategy=$liblayout"; strand-non-specific"
                        # test if strand is double
                        elif [[ "$strand" == "double" ]]
                        then
                            # libstrategy: liblayout (single/paired end); manual search for strand information
                            libstrategy=$liblayout"; look into characteristics and/or extraction for strand information"
                        else
                            # no information about strand
                            # libstrategy: liblaylout (single/paired end)
                            libstrategy=$liblayout
                        fi
                    fi

                    # liblayout as in FungiNet is not available
                    liblayout="NA"

                    # srr: SRR accession
                    srr=$(grep "RUN_SET" < $srxfile | awk -F"<RUN_SET>" '{ print $2 }' | awk -F"<PRIMARY_ID>" '{ print $2 }' | awk -F"</PRIMARY_ID>" '{ print $1 }')
                    echo "SRR: $srr" >> $log

                    # seqstoragedate: Storage Date (empty, because no download till now)
                    seqstoredate=""

                    # seqstoragelocation: Storage Path (empty, because no download till now)
                    seqstorelocation=""

                    # just output if SRR available
                    if [[ $srr == *SRR* ]]
                    then
                        # outputfile: manipulating the original output path, insert strain
                        outputfile=$(echo $output | awk -v strain="$strain_f" -F";" '{ print $1strain$3strain$5 }')
                        # writing all information to outputfile
                        echo -e "$gse\t$gse_title\t$pubmed\t$gse_status\t$gse_lastupdate\t"$gse_summary"\t$gse_overall\t"$gse_author"\t"$gse_contact"\t"$gse_contrib"\t$GSM\t$specimen\t"$characteristics"\t$strain\t"$organism"\t"$taxid"\t$treat\t$growth\t$mt\t"$extraction"\t$instrument\t$libsource\t$libstrategy\t$libselection\t$liblayout\t$srx_acc\t$srr\t$seqstoredate\t$seqstorelocation" >> $outputfile
                    fi

                    # get into GSE folder
                    cd ".."

                done
            fi

            # get into temp folder
            cd ".."
        done

        # get into organism folder
        cd ".."

        # path for helpdate file used for checking if there are older runs
        helpdate=$(pwd)
        helpdate=$helpdate"/".helpdate.txt

        # remove temp or keep for debugging?
        if [[ $debug == 0 ]]
        then
            rm -rf temp 2>/dev/null
        fi

        # for every strain directory
        for i in $(ls -d */)
        do
            # i: strain
            i=$(echo $i | tr -d "/")

            # get into strain folder
            cd $i

            # build output paths
            # outputfile: manipulating the original output path, insert strain
            outputfile=$(echo $output | awk -v strain="$i" -F";" '{ print $1strain$3strain$5 }')
            touch $outputfile
            # alltsvfile: manipulating the original alltsv path, insert strain
            alltsvfile=$(echo $all_tsv | awk -v strain="$i" -F";" '{ print $1strain$3strain$5 }')

            # if there is no helpdate file (there was no run before)
            if [ ! -f $helpdate ]
            then
                # if there is an outputfile, which is not empty
                if [[ -s $outputfile ]]
                then
                    # get header into alltsvfile
                    echo -e "Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \tRNA Extraction \t \tSequencing \t \t \t \t \t \t \t \t" >> $alltsvfile
                    echo -e "GSE\tGSE Title\tPubmed ID\tGSE Status\tGSE Last Update\tGSE Summary\tGSE Overall Design\tGSE Author\tGSE Contact\tGSE Contributors\tGSM\tSpecimen\tCharacteristics\tStrain\tOrganism\tTaxID\tTreatment Protocol\tGrowth Protocol\tMaterial Type\tExtraction Protocol\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSRX Acc\tSRR Acc\tStorage Date\tData File" >> $alltsvfile
                    # copy findings into alltsvfile
                    cat $outputfile >> $alltsvfile
                fi
            else
                # if there is an outputfile, which is not empty
                if [[ -s $outputfile ]]
                then
                    # copy findings into alltsvfile
                    cat $outputfile >> $alltsvfile
                fi
            fi

            # if there is no outputfile or this file is empty
            if [[ ! -s $outputfile ]]
            then
                # remove outputfile
                rm $outputfile
            else
                # copy header to new findings file (outputfile)
                sed -i '1s/^/GSE\tGSE Title\tPubmed ID\tGSE Status\tGSE Last Update\tGSE Summary\tGSE Overall Design\tGSE Author\tGSE Contact\tGSE Contributors\tGSM\tSpecimen\tCharacteristics\tStrain\tOrganism\tTaxID\tTreatment Protocol\tGrowth Protocol\tMaterial Type\tExtraction Protocol\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSRX Acc\tSRR Acc\tStorage Date\tData File\n/' $outputfile
                sed -i '1s/^/Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t RNA Extraction \t \t Sequencing \t \t \t \t \t \t \t \t\n/' $outputfile
            fi

            # get into Organism folder
            cd ".."
        done

        ###################
        ###################
        ###################

        # writing helpdate file
        echo "$today" > .helpdate.txt

        # after: date after whole script
        after=$(date +%s)

        # output runtime for creating table
        echo "Time for generating metadata table: " $((after - $afterdown)) "seconds"

        # output runtime together
        echo "Elapsed time: " $((after - $before)) "seconds"

    # if there is no data available
    else
        # change to organism directory
        cd ".."

        # writing helpdate file
        echo "$today" > .helpdate.txt

        # straindircount: help variable counting directories in organism folder
        straindircount=$(ls -1 -p | grep "/" | wc -l)

        # if there is already data (numbers of directory is at least 2)
        if [[ $straindircount -ge 2 ]]
        then
            # output, that there is no new data
            echo "There was no new RNA-seq data available for $organism"
            # remove temp
            rm -rf temp 2>/dev/null
        else
            # it was a new run, but there is no new data

            # change to start directory
            cd ".."

            # remove created folders
            rm -rf $orgfolder 2>/dev/null

            # output indicating there is no data
            echo "There was no RNA-seq data available for $organism"
        fi
    fi
else
    # if there are to many arguments supplied
    echo "too many arguments supplied"
fi
