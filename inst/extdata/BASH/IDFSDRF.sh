#!/bin/bash

## Informations
#  title            : IDFSDRF.sh
#  description      : This script creates a metadata table for available
#                     RNA-seq data in GEO (Gene Expression Omnibus) for
#                     a given organism
#  author           : Silvia Gerber (née Mueller)
#  date             : 2015/04/10 (minor change 2015/11/30)
#  usage            : bash IDFSDRF.sh /full/Path/to/storage/directory /full/path/to/destination/directory
#  usage example    : bash IDFSDRF.sh /home/silvia/RNA-Seq/ "/home/silvia/RNA-Seq/Metadata"

# month_to_num: function to convert abbreviation for month to numerical output
function month_to_num {
    case $1 in
        Jan)
            month="01";;
        Feb)
            month="02";;
        Mar)
            month="03";;
        Apr)
            month="04";;
        Mai)
            month="05";;
        Jun)
            month="06";;
        Jul)
            month="07";;
        Aug)
            month="08";;
        Sep)
            month="09";;
        Oct)
            month="10";;
        Nov)
            month="11";;
        Dec)
            month="12";;
    esac
}

# convert_date_full: function for extracting year(yyyy), day(dd), month(mm) from default system date format
# input example: Wed Dec 3 14:33:55 CET 2014
# if day is single-digit, prefix it with 0
function convert_date_full {
    year=$(echo "$1" | awk '{ print $6 }')
    day=$(echo "$1" | awk '{ print $3 }')
    if (( 0<=$day && $day<=9 ))
    then
        day="0"$day
    fi
    month=$(echo "$1" | awk '{ print $2 }')
    month_to_num $month
}

# convert_date_GEO: function for extracting year(yyyy), day(dd), month(mm) from GEO public date
# or other schemes like "Public on Jun 16 2011"
function convert_date_GEO {
    year=$(echo "$1" | awk '{ print $5 }')
    day=$(echo "$1" | awk '{ print $4 }')
    month=$(echo "$1" | awk '{ print $3 }')
    month_to_num $month
}

# to_SDRF: function for transforming general metadata table to SDRF format
function to_SDRF {
    # tsv: argument (given tsv-file)
    tsv=$1
    # extracting path from argument
    path_tsv=$(echo $tsv | awk -F"/" '{ for(i=1;i<NF;i++) printf "%s",$i OFS; if (NF-1) "%s";$(NF-1); printf ORS }' OFS="/")
    # extracting organism from argument
    org=$(echo $tsv | awk -F"/" '{ print $NF }' | awk -F".tsv" '{ print $1 }')

    # path to output file
    sdrf=$path_tsv$org"_SDRF.tsv"

    # output header to output
    echo -e "Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t RNA Extraction \t \t \t \t \t \t \t \t \t \t \t \t \t \t \tSequencing \t \t \t \t \t \t \t \t \t \t \t \t \t \tData Processing \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \tExperimental Factor Values \t \t \t \tTerm Source" >> $sdrf
    echo -e "Specimen\tMaterial Typ\tOrganism\tStrain or Line\tOrganism Part\tOrganism Status\tSex\tAge\tAge Time Unit\tSampling Time Point\tSampling Time Point Unit\tTemperature\tTemperature Unit\tpH\tAtmosphere\tCompound\tMedia\tNutrients\tStorage Protocol\tStorage Date\tStorage Location\tExtraction Protocol\tExtract Name\t260/280\t230/280\tRIN/RNA integrity number\tConcentration\tUnit\tVolume\tUnit\tExtraction Kit\tMaterial Type\tExtraction Date\tStorage Protocol\tStorage Date\tStorage Location\tSequencing Protocol\tAssay Name\tSequencing Performer\tTechnology Type\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSequencing Date\tStorage Protocol\tStorage Date\tStorage Location\tData File\tData Processing Protocol\tData Processing Software\tParameters And Values\tData Processing Protocol\tData Processing Software\tParameters And Values\tData Processing Protocol\tData Processing Software\tParameters And Values\tData Processing Protocol\tData Processing Software\tParameters And Values\tData Processing Protocol\tData Processing Software\tParameters And Values\tFold Change Assignment\tData Processing Protocol\tAnnotation Source\tAnnotation Version\tAnnotation Date\tAnnotation Data File\tStorage Protocol\tStorage Date\tStorage Location\tDerived Data File\t\t\t\t\tTerm Source" >> $sdrf

    # loop through every line in tsv file except header (first two lines)
    awk 'NR>2 {print}' $tsv | while read line
    do
        # extract SRR accession for check if already in SDRF
        SRR=$(echo -e "$line" | awk -F"\t" '{ print $27 }')
        # help2: test variable if this entry is already in SDRF
        help2=$(if grep -q -m 1 $SRR $sdrf; then echo "1"; else echo "0"; fi)
        # if this entry is not already in SDRF, build SDRF entry
        if [[ $help2 == 0 ]]
        then
            # extracted specimen information
            # @sed: extract trailing
            source_specimen=$(echo "$line" | awk -F "\t" '{ print $12 }' | sed -e 's/^ *//' -e 's/ *$//' | tr " " "_")
            #source_specimen=$(echo "$source_specimen" | awk -F"tle_=_" '{ print $2 }')
            # always "whole_organism"
            source_material_type="whole_organism"
            # extracted organism
            source_organism=$(echo "$line" | awk -F"\t" '{ print $15 }')
            # formatting organism to possible values of table
            case $source_organism in
            "Aspergillus fumigatus")
                source_organism="Aspergillus fumigatus";;
            "Candida albicans")
                source_organism="Candida albicans";;
            "Candida dubliniensis")
                source_organism="Candida dubliniensis";;
            #*)
            #    source_organism="other";;
            esac

            # extracted strain
            source_strain=$(echo "$line" | awk -F"\t" '{ print $14 }')
            # formatting strain to possible values of table

            # organism part is unknown, maybe manually
            source_organism_part="other"
            # organism status is unknown, maybe manually
            source_organism_status="other"
            # sex is always not applicable
            source_sex="not applicable"
            # age is unknown
            source_age=""
            # age unknown, therefor unit unknown
            source_age_unit="other"
            # sampling time point unknown, maybe manually
            source_sampling_tp=""
            # sampling time point unknown, therefor unit unknown
            source_sampling_tp_unit="other"
            # temperature unknown, maybe manually
            source_temperature=""
            # temperature unknown, therefor unit unknown
            source_temperature_unit="other"
            # pH unknown, maybe manually
            source_ph=""
            # atmosphere unknown, maybe manually
            source_atmosphere="other"
            # compount unknown, maybe manually
            source_compound="other"
            # media unknown, maybe manually
            source_media="other"
            # nutrients unknown, maybe manually
            source_nutrients="other"
            # no storage
            source_storage_prot="NO STORAGE"
            source_storage_date=""
            source_storage_location="no storage"

            rna_extraction_prot="RNA EXTRACTION"
            rna_extract_name=$(echo $source_specimen"_e")
            # 260/280 Ratio unknown
            rna_twosix=""
            # 230/280 Ratio unknown
            rna_twothree=""
            # RNI/RNA integrity number unknown
            rna_RIN=""
            # concentration unknown
            rna_concentration=""
            # concentration unknown, therefor unit unknown
            rna_concentration_unit="other"
            # volume unknown
            rna_volume=""
            # volume unknown therefor unit unknown
            rna_volume_unit="other"
            # extraction kit always the same
            rna_extraction_kit="RNA extraction kit"
            # material type is always "total_RNA"
            rna_material_type="total_RNA"
            # extraction date unknown
            rna_extraction_date=""
            # no storage
            rna_storage_prot="NO STORAGE"
            rna_storage_date=""
            rna_storage_location="no storage"

            seq_sequencing_prot="SEQUENCING"
            seq_assay_name=$(echo $source_specimen"_s")
            # performer unknown, maybe manually
            seq_sequencing_performer="other"
            # extracted instrument
            seq_sequencing_instrument=$(echo "$line" | awk -F"\t" '{ print $21 }')
            # regular expressions for instrument for getting technology type
            illumina=".*[iI][lL][lL][uU][mM][iI][nN][aA].*"
            solid=".*[sS][oO][lL][iI][dD].*"
            fourfivefour=".*454.*"
            pacific=".*[pP][aA][cC][iI][fF][iI][cC].*"
            # getting technology type
            if echo $seq_sequencing_instrument | grep -Eq "$illumina"
            then
                seq_technology_type="Sequencing by synthesis (Illumina)"
            elif echo $seq_sequencing_instrument | grep -Eq "$solid"
            then
                seq_technology_type="Sequencing by ligation (SOLiD sequencing)"
            elif echo $seq_sequencing_instrument | grep -Eq "$fourfivefour"
            then
                seq_technology_type="Pyrosequencing (454)"
            elif echo $seq_sequencing_instrument | grep -Eq "$pacific"
            then
                seq_technology_type="Single-molecule real-time sequencing (Pacific Bio)"
            else
                seq_technology_type="other"
            fi
            # extracted library source
            seq_lib_source=$(echo "$line" | awk -F"\t" '{ print $22 }')
            # extracted library strategy, wrong format, in best case there are 3 informations (single/paired, strand specific, polyA)
            seq_lib_strategy=$(echo "$line" | awk -F"\t" '{ print $23 }')
            # single or paired end information should always be in the first field
            field1=$(echo "$seq_lib_strategy" | awk -F'[;,]' '{ print $1 }')
            field2=$(echo "$seq_lib_strategy" | awk -F'[;,]' '{ print $2 }')
            field3=$(echo "$seq_lib_strategy" | awk -F'[;,]' '{ print $3 }')
            singlepaired="$field1"
            # test what information is in second field and formatting information
            case $field2 in
                "poly(A)")
                    polya="poly-A filter";;
                "strand-specific")
                    strand="strand specific";;
                *)
                    polya="NA"
                    strand="NA";;
            esac
            # test what information is in third field and formatting information
            case $field3 in
                "poly(A)")
                    polya="poly-A filter";;
                "strand-specific")
                    strand="strand specific";;
                *)
                    polya="NA"
                    strand="NA";;
            esac
            # assumption, if there is no information about poly-A filter, there is poly-A filter
            if [[ "$polya" == *NA* ]]
            then
                polya="poly-A filter"
            fi
            # assumption, if there is no information about strand-specificity, it is unspecific
            if [[ "$strand" == *NA* ]]
            then
                strand="unspecific"
            fi
            # formatted library strategy
            seq_lib_strategy="$singlepaired;$strand;$polya"
            # extracted library selection
            seq_lib_selection=$(echo "$line" | awk -F"\t" '{ print $24 }')
            # extracted library layout
            seq_lib_layout=$(echo "$line" | awk -F"\t" '{ print $25 }')
            # no date availabe
            seq_sequencing_date=""
            # storage informations
            seq_storage_prot="STORAGE"
            # storage date = download date; formatted to yyyy-mm-dd
            seq_storage_date=$(echo "$line" | awk -F"\t" '{print $28}')
            # if storage date is empty, no use of function
            if [[ $seq_storage_date == "" ]]
            then
                seq_storage_date=""
            else
                convert_date_full "$seq_storage_date" 2>/dev/null
                seq_storage_date=$year"-"$month"-"$day
            fi
            seq_storage_date=$year"-"$month"-"$day
            # formatting if there is more than one file (paired end)
            seq_storage=$(echo "$line" | awk -F"\t" '{ print $29 }')
            if echo $seq_storage | grep -Eq ";"
            then
                seq_storage_file=""
                IFS=";"
                for i in $seq_storage
                do
                    f=$(echo $i | awk -F"/" '{ print $NF }')
                    seq_storage_file=$seq_storage_file"; "$f
                done
                seq_storage_file=${seq_storage_file:2}
                unset IFS
            # formatting if there is just one (single-end) file
            else
                seq_storage_file=$(echo $seq_storage | awk -F"/" '{ print $NF }')
            fi

            # storage_location: whole path to directory with sequence files
            strain=$(echo "$line" | awk -F"\t" '{ print $14 }')
            seq_storage=$(echo "$line" | awk -F"\t" '{print $29}')
            if [[ $seq_storage == "" ]]
            then
                seq_storage_location=""
            else
                seq_storage_location=$(echo "$seq_storage" | awk -F";" '{ print $1 }' | awk -F"$strain" '{ print $1}' )
                seq_storage_location=$seq_storage_location$strain"/"
            fi
            # output all informations
            echo -e $source_specimen"\t"$source_material_type"\t"$source_organism"\t"$source_strain"\t"$source_organism_part"\t"$source_organism_status"\t"$source_sex"\t"$source_age"\t"$source_age_unit"\t"$source_sampling_tp"\t"$source_sampling_tp_unit"\t"$source_temperature"\t"$source_temperature_unit"\t"$source_ph"\t"$source_atmosphere"\t"$source_compound"\t"$source_media"\t"$source_nutrients"\t"$source_storage_prot"\t"$source_storage_date"\t"$source_storage_location"\t"$rna_extraction_prot"\t"$rna_extract_name"\t"$rna_twosix"\t"$rna_twothree"\t"$rna_RIN"\t"$rna_concentration"\t"$rna_concentration_unit"\t"$rna_volume"\t"$rna_volume_unit"\t"$rna_extraction_kit"\t"$rna_material_type"\t"$rna_extration_date"\t"$rna_storage_prot"\t"$rna_storage_date"\t"$rna_storage_location"\t"$seq_sequencing_prot"\t"$seq_assay_name"\t"$seq_sequencing_performer"\t"$seq_technology_type"\t"$seq_sequencing_instrument"\t"$seq_lib_source"\t"$seq_lib_strategy"\t"$seq_lib_selection"\t"$seq_lib_layout"\t"$seq_sequencing_date"\t"$seq_storage_prot"\t"$seq_storage_date"\t"$seq_storage_location"\t"$seq_storage_file"\tQUALITY CONTROL OF RAW DATA\tFastQC\t \tQUALITY TRIMMING OF RAW DATA\tbtrim\t \tMAPPING\ttophat2\t \tCOUNTING\t\t\tDIFFERENTIAL EXPRESSION\t\t\t\tAnnotation\t\t\t\t\tSTORAGE\t\t\t\t\t\t\t\tMGED Ontology" >> $sdrf
        fi
    done
}

# to_IDF: function for formatting general metadata table to IDF format
function to_IDF {
    # tsv: the argument is the tab-delimited general metadata table (tsv file)
    tsv=$1
    # extracting path from argument
    path_tsv=$(echo $tsv | awk -F"/" '{ for(i=1;i<NF;i++) printf "%s",$i OFS; if (NF-1) "%s";$(NF-1); printf ORS }' OFS="/")
    # extracting organism from argument
    org=$(echo $tsv | awk -F"/" '{ print $NF }' | awk -F".tsv" '{ print $1 }')

    # path to output file
    idf=$path_tsv$org"_IDF.tsv"

    # line: third line (first entry) in $tsv file for extracting information
    line=$(awk 'NR==3 {print}' $tsv)
    # GSE: GSE accession number (extracted from file name)
    GSE=$(echo "$line" | awk -F"\t" '{ print $1 }' | tr -d " ")
    # SRR: SRR accession number (extracted from file name)
    SRR=$(echo "$line" | awk -F"\t" '{ print $27 }'| tr -d " ")

    ##### Start creating IDF file #################

    # output formal lines
    echo "# Template submission sheet for FungiNet RNA-seq experiments" >> $idf
    echo "" >> $idf
    echo "# This section contains the top-level information for your experiment" >> $idf
    # gse_title: extracted GSE Title
    gse_title=$(echo "$line" | awk -F"\t" '{ print $2 }'| awk -F";" '{ print $1 }')
    echo -e "Investigation Title \t"$gse_title >> $idf
    # exp_class: allways RNA-seq
    exp_class="RNA-seq"
    echo -e "Experiment Class \t"$exp_class >> $idf
    # gse_summary: extracted summary for GSE Experiment
    gse_summary=$(echo "$line" | awk -F"\t" '{ print $6 }' | awk -F";" '{ print $1 }')
    echo -e "Experiment Description \t"$gse_summary >> $idf
    # exp_design: information not automatically, therefor other (manually)
    exp_design="other"
    echo -e "Experimental Design \t"$exp_design >> $idf

    echo "" >> $idf

    echo "# 'Experimental Factors' used to describe the variables investigated by your experiment" >> $idf
    # exp_factor_name: no automatic information; manually
    exp_factor_name=""
    echo -e "Experimental Factor Name \t"$exp_factor_name >> $idf
    # exp_factor_type: no automatic information; manually
    exp_factor_type=""
    echo -e "Experimental Factor Type \t"$exp_factor_type >> $idf

    echo "" >> $idf

    echo "# Quality control information" >> $idf
    # quality_control_type: no automatic information; manually
    quality_control_type="other"
    echo -e "Quality Control Type \t"$quality_control_type >> $idf

    echo "" >> $idf

    echo "# Data release information" >> $idf
    # public_release: release date in GEO
    public_release=$(echo "$line" | awk -F"\t" '{ print $4 }' | awk -F";" '{ print $1 }')
    # convert the release date information from GEO to yyyy-mm-dd
    convert_date_GEO "$public_release"
    public_release_date=$year"-"$month"-"$day
    echo -e "Public Release Date \t"$public_release_date >> $idf
    # release_database: GEO (SRA would be possible too, but more metadata information from GEO)
    release_database="GEO"
    echo -e "Database \t"$release_database >> $idf
    # GSE: GSE accession number
    GSE=$(echo "$line" | awk -F"\t" '{ print $1 }' | tr -d " ")
    # GSM: GSM accession number
    GSM=$(echo "$line" | awk -F"\t" '{ print $11 }' | tr -d " ")
    echo -e "Accession Number \t"$GSM","$GSE >> $idf

    echo "" >> $idf

    echo "# Personal information about the data submitter(s) and investigator(s)" >> $idf
    # contact: extracted contact information for GSE, containing name; email; phone; institute; address; city; state; zip/postal_code; country
    contact=$(echo "$line" | awk -F"\t" '{ print $9 }')
    # name: extracted name of submitter; first letter is upper case
    name=$(echo $contact | awk -F"name : " '{ print $2 }' | awk -F";" '{ print $1 }')
    name="$(tr '[:lower:]' '[:upper:]' <<< ${name:0:1})${name:1}"
    # last_name: extracted last name of submitter; first letter is upper case
    last_name=$(echo $name | awk -F"," '{ print $3 }')
    last_name="$(tr '[:lower:]' '[:upper:]' <<< ${last_name:0:1})${last_name:1}"
    echo -e "Person Last Name \t"$last_name >> $idf
    # first_name: extracted first name of submitter
    first_name=$(echo $name | awk -F"," '{ print $1 }')
    echo -e "Person First Name \t"$first_name >> $idf
    # mid_initials: extracted mid initials of name of submitter; transformed to upper case
    mid_initials=$(echo $name | awk -F"," '{ print $2 }')
    mid_initials=$(echo $mid_initials | tr '[:lower:]' '[:upper:]')
    echo -e "Person Mid Initials \t"$mid_initials >> $idf
    # email: extracted email of submitter
    email=$(echo $contact | awk -F"email : " '{ print $2 }' | awk -F";" '{ print $1 }')
    echo -e "Person Email \t"$email >> $idf
    # phone: no information availabe
    phone=""
    echo -e "Person Phone \t"$phone >> $idf
    # address: extracted address of submitter from contact field, contains mainly street
    address=$(echo $contact | awk -F"address : " '{ print $2 }' | awk -F";" '{ print $1 }')
    # city: extracted from contact field
    city=$(echo $contact | awk -F"city : " '{ print $2 }' | awk -F";" '{ print $1 }')
    # state: extracted from contact field (in Germany sometimes missing)
    state=$(echo $contact | awk -F"state : " '{ print $2 }' | awk -F";" '{ print $1 }')
    # zip: extracted from contact field
    zip=$(echo $contact | awk -F"postal_code : " '{ print $2 }' | awk -F";" '{ print $1 }')
    # country: extracted from contact field
    country=$(echo $contact | awk -F"country : " '{ print $2 }' | awk -F";" '{ print $1 }')
    # person_address: newly build address information
    person_address="Address: "$address"; Zip: "$zip"; City: "$city"; State: "$state"; Country: "$country
    echo -e "Person Address \t"$person_address >> $idf
    # regular expression for HKI, FSU, JMU
    HKIregex=".*(HKI|Hans[- ]?Knöll[- ]?Institut).*"
    FSUregex=".*(FSU|Friedrich[- ]?Schiller[- ]?(University|Universität)).*"
    JMUregex=".(JMU|Julius[- ]?Maximilians[- ]?(University|Universität)).*"
    # testing if the institute of submitter is available in table, otherwise institute is "other"
    institute=$(echo $contact | awk -F"institute : " '{ print $2 }' | awk -F";" '{ print $1 }')
    if echo -e "$institute" | grep -Eq "$HKIregex"
    then
        institute="Hans-Knöll-Institute Jena"
    elif echo -e "$institute" | grep -Eq "$FSUregex"
    then
        institute="Friedrich Schiller University Jena"
    elif echo -e "$institute" | grep -Eq "$JMUregex"
    then
        institute="Julius Maximilians University Würzburg"
    else
        institute="other"
    fi
    echo -e "Person Affiliation - Institution \t"$institute >> $idf
    # if institute is known, department is "other", otherwise department is "not applicable"
    if [[ "$institute" == "other" ]]
    then
        department="not applicable"
    else
        department="other"
    fi
    echo -e "Person Affiliation - Department \t"$department >> $idf
    # roles: assumption it is always submitter
    roles="submitter"
    echo -e "Person Roles \t"$roles >> $idf
    echo "" >> $idf
    # no information about cooperation partners availabe"
    echo "Cooperation partners within FungiNet - Institution" >> $idf
    echo "Cooperation partners within FungiNet - Department" >> $idf
    echo "Cooperation partners within SysMoDB" >> $idf
    echo "Other cooperation partners" >> $idf

    echo "" >> $idf

    echo "# Publications based on the data described in this file" >> $idf
    # pubmed: Pubmed ID
    pubmed=$(echo "$line" | awk -F"\t" '{ print $3 }' | tr -d " " )
    # remove last character of pubmed (in table of getMeta.sh there is always an ";" at the end)
    pubmed="${pubmed%?}"
    # test: help variable to count number of Pubmed IDs
    test=$(echo $pubmed | awk -F";" '{ print NF }' )
    # test if there is a Pubmed ID (sometimes NA)
    if [[ $pubmed == *N* ]]
    then
        # if there is no Pubmed ID, output is always "NA"
        echo -e "PubMed ID \tNA">> $idf
        echo -e "Publication DOI or Journal/Book Title \tNA" >> $idf
        echo -e "Publication Author List \tNA" >> $idf
        echo -e "Publication Title List \tNA" >> $idf
        echo -e "Publication Status \tother" >> $idf
    # test if there is at least one Pubmed ID
    elif [ $test -ge 0 ]
    then
        # i: counting variable to loop through all Pubmed IDs
        i=1
        # basicURLeUtils: URL for using eutils
        basicURLeUtils="eutils.ncbi.nlm.nih.gov/entrez/eutils/"
        # creating empty lists
        pubmed_id_list=""
        pubmed_doi_list=""
        pubmed_author_list=""
        pubmed_title_list=""
        pubmed_status_list=""
        # loop through all Pubmed IDs
        while [ $i -le $test ]
        do
            # pubmed_id: current Pubmed ID
            pubmed_id=$(echo "$pubmed" | awk -F";" -v i="$i" '{ print $i }')
            # pubmed_id_list: append list by current pubmed_id
            pubmed_id_list=${pubmed_id_list}${pubmed_id}"\t"
            # building pubmed_url
            pubmed_url=$basicURLeUtils"esummary.fcgi?db=pubmed&id="$pubmed_id
            # building output file name
            pubmed_xml=$pubmed_id".xml"
            # getting information file for current pubmed id
            wget -q -O $pubmed_xml $pubmed_url
            # extracting doi from information file
            pubmed_doi=$(grep "<Item Name=\"DOI\"" $pubmed_xml | awk -F">" '{ print $2 }' | awk -F"<" '{ print $1 }')
            # append list by doi of current pubmed_id
            pubmed_doi_list=${pubmed_doi_list}${pubmed_doi}"\t"
            # extracting author from information file
            pubmed_author=$(grep "\"Author\"" $pubmed_xml | awk -F">" '{ print $2 }' | awk -F"<" '{ print $1";" }')
            # append list by author of current pubmed_id
            pubmed_author_list=${pubmed_author_list}${pubmed_author}"\t"
            # extracting title from information file
            pubmed_title=$(grep "\"Title\"" $pubmed_xml | awk -F">" '{ print $2 }' | awk -F"<" '{ print $1 }')
            # append list by title of current pubmed_id
            pubmed_title_list=${pubmed_title_list}${pubmed_title}"\t"
            # pubmed_status: is published if there is a pubmed id
            pubmed_status="published"
            # append list by status of current pubmed_id
            pubmed_status_list=${pubmed_status_list}${pubmed_status}"\t"
            # rm information file
            rm $pubmed_xml
            # increment counter
            ((i++))
        done
        # output informations for PubMed
        echo -e "PubMed ID \t""${pubmed_id_list}" >> $idf
        echo -e "Publication DOI or Journal/Book Title \t""${pubmed_doi_list}" >> $idf
        echo -e "Publication Author List \t"${pubmed_author_list} >> $idf
        echo -e "Publication Title \t""${pubmed_title_list}" >> $idf
        echo -e "Publication Status \t""${pubmed_status_list}" >> $idf
    fi

    echo "" >> $idf

    echo "# Protocols (Standard Operation Procedures, SOPs)" >> $idf
    # output headers for next section
    echo -e "#\tExtraction Protocols\tSequencing Protocols\tData Processing Protocols\t\t\t\t\t\tStorage Protocols\t" >> $idf
    echo -e "Protocol Name\tRNA EXTRACTION\tSEQUENCING\tQUALITY CONTROL OF RAW DATA\tQUALITY TRIMMING OF RAW DATA\tMAPPING\tCOUNTING\tDIFFERENTIAL EXPRESSION\tANNOTATION\tSTORAGE\tNO STORAGE" >> $idf
    echo -e "Protocol URI\trna_extraction\tsequencing\tqc_raw\tqt_raw\tmapping\tcounting\tdiff_exp\tannotation\tstorage\tno_storage" >> $idf
    echo -e "Protocol Type\tnucleic_acid_extraction\tunknown_protocol_type\tunkown_protocol_type\tunknown_protocol_type\tunknown_protocol_type\tunknown_protocol_type\tunknown_protocol_type\tunknown_protocol_type\tstore\tstore" >> $idf
    # extraction: extracted details for "RNA extraction"
    extraction=$(echo "$line" | awk -F"\t" '{ print $ 20 }')
    # sequencing: default
    sequencing="details of the sequencing process"
    # quality_contol: default
    quality_control="first step of data processing"
    # quality_trimming: default
    quality_trimming="second step of data processing"
    # mapping: default
    mapping="third step of data processing"
    # counting: default
    counting="forth step of data processing"
    # diff_exp: default
    diff_exp="details of filtering for differentially regulated genes"
    # annotation: default
    annotation="functional annotation of all genes in the dataset"
    # storage: default
    storage="storage of data"
    # no_storage: default
    no_storage="no storage of data"
    # output informations above
    echo -e "Protocol Description \t"$extraction"\t"$sequencing"\t"$quality_control"\t"$quality_trimming"\t"$mapping"\t"$counting"\t"$diff_exp"\t"$annotation"\t"$storage"\t"$no_storage >> $idf
    # seq_sequencing_instrument: extracted instrument information
    seq_sequencing_instrument=$(echo "$line" | awk -F"\t" '{ print $21 }')
    # output of rest default informations
    echo -e "Protocol Parameters \t260/280; 230/280; RIN/RNA integrity number; Concentration; Volume; Extraction kit \tPerformer; Technology Type; Library Source; Library Strategy; Library Selection; Library Layout \t\t\t\t\t\tVersion of Annotation\t\t" >> $idf
    echo -e "Protocol Hardware \t \t$seq_sequencing_instrument" >> $idf
    echo -e "Protocol Software \t \t \t \t \t \t \t \t \t \t" >> $idf

    echo "" >> $idf

    echo "# Default values for filling in data automatically in the SDRF" >> $idf

    echo "" >> $idf

    # biobank_mt: always "whole_organism"
    biobank_mt="whole_organism"
    echo -e "# Default Biobank Material Type \t"$biobank_mt >> $idf
    # organism: extracted organism
    organism=$(echo "$line" | awk -F"\t" '{ print $15 }')
    # not all fungi are in list, so if in list, output organism, if not in list "other"
    case $organism in
        "Aspergillus fumigatus")
            organism="Aspergillus fumigatus";;
        "Candida albicans")
            organism="Candida albicans";;
        "Candida dubliniensis")
            organism="Candida dubliniensis";;
        #*)
        #    organism="other";;
    esac
    echo -e "# Default Organism \t"$organism >> $idf
    # extract strain information
    strain=$(echo "$line" | awk -F"\t" '{ print $14 }')
    # if organism is C. albicans
    if [[ $organism == *albicans* ]]
    then
        # test if strain is something with WO1 or SC5314, otherwise strain is "other"
        case $strain in
            WO1)
                # output formatted WO1 strain
                strain="MTL strain WO-1";;
            SC5314)
                strain="SC5314";;
            #*)
            #    strain="other";;
        esac
    # if organism is A. fumigatus
    elif [[ $organism == *fumigatus* ]]
    then
        # our strain CEA10 is not in list, so strain is "other"
        case $strain in
        #    *)
        #        strain="other";;
        esac
    # if organism is C. dubliniensis
    elif [[ $organism == *dubliniensis* ]]
    then
        # no strains in table availabe
        case $strain in
        #    *)
        #        strain="not applicable";;
        esac
    # if organism is "other"
    elif [[ $organism == *other* ]]
    then
        # no strains in table availabe
        case $strain in
        #    *)
        #        strain="not applicable";;
        esac
    fi
    echo -e "# Default Strain or Line \t"$strain >> $idf
    # no information about organism part, maybe sometimes manually
    organism_part="other"
    echo -e "# Default Organism Part \t"$organism_part >> $idf
    # no information about organism status, maybe sometimes manually
    organism_status="other"
    echo -e "# Default Organism Status \t"$organism_status >> $idf
    # sex: always "not applicable" because fungi
    sex="not applicable"
    echo -e "# Default Sex \t"$sex >> $idf
    # no information about age
    age=""
    echo -e "# Default Age \t"$age >> $idf
    # because there is no information about age, there is no unit availabe
    age_unit="other"
    echo -e "# Default Age Time Unit \t"$age_unit >> $idf
    # no information about sampling time point, maybe sometimes manually
    sampling_time_point=""
    echo -e "# Default Sampling Time Point \t"$sampling_time_point >> $idf
    # because there is no information about the sampling time point, there is no unit availabe
    sampling_time_point_unit="other"
    echo -e "# Default Sampling Time Point Unit \t"$sampling_time_point_unit >> $idf
    # no information about temperatur, maybe sometimes manually
    temperature=""
    echo -e "# Default Temperature \t"$temperature >> $idf
    # because there is no information about the temperature, there is no unit availabe
    temperature_unit="other"
    echo -e "# Default Temperature Unit \t"$temperature_unit >> $idf
    # no information about pH, maybe sometimes manually
    ph=""
    echo -e "# Default pH \t"$ph >> $idf
    # no information about atmosphere, maybe sometimes manually
    atmosphere="other"
    echo -e "# Default Atmosphere \t"$atmosphere >> $idf
    # no information about compund, maybe sometimes manually
    compound="other"
    echo -e "# Default Compound \t"$compound >> $idf
    # no information about medium, maybe sometimes manually
    medium="other"
    echo -e "# Default Medium \t"$medium >> $idf
    # no information about nutrients, maybe sometimes manually
    nutrient="other"
    echo -e "# Default Nutrient \t"$nutrient >> $idf
    # no storage of biobank material
    biobank_storage_protocol="NO STORAGE"
    echo -e "# Default Biobank Storage Protocol \t"$biobank_storage_protocol >> $idf
    # no storage, so there is no date
    biobank_storage_date=""
    echo -e "# Default Biobank Storage Date \t"$biobank_storage_date >> $idf
    # no storage, so location is no storage
    biobank_storage_location="no storage"
    echo -e "# Default Biobank Storage Location \t"$biobank_storage_location >> $idf
    # rna_extraction_protocol: always "RNA EXTRACTION"
    rna_extraction_protocol="RNA EXTRACTION"
    echo -e "# Default RNA Extraction Protocol \t"$rna_extraction_protocol >> $idf
    # 260/230 Ratio: no information available
    twosixty=""
    echo -e "# Default 260/280 Ratio \t"$twosixty >> $idf
    # 230/280 Ratio: no information availabe
    twothirty=""
    echo -e "# Default 230/280 Ratio \t"$twothirty >> $idf
    # RIN/RNA integrity number: no information available
    rinrna=""
    echo -e "# Default RIN/RNA integrity number \t"$rinrna >> $idf
    # RNA concentration: no information available
    rna_concentration=""
    echo -e "# Default RNA Concentration \t"$rna_concentration >> $idf
    # because there is no concentration available, there is no unit
    rna_concentration_unit="other"
    echo -e "# Default RNA Concentration Unit \t"$rna_concentration_unit >> $idf
    # no information available for volume
    rna_volume=""
    echo -e "# Default RNA Volume \t"$rna_volume >> $idf
    # because there is no information about volume, there is no unit
    rna_volume_unit="other"
    echo -e "# Default RNA Volume Unit \t"$rna_volume_unit >> $idf
    # always RNA extraction kit
    rna_extraction_kit="RNA extraction kit"
    echo -e "# Default RNA Extraction Kit \t"$rna_extraction_kit >> $idf
    #rna_material_type=$(echo "$line" | awk -F"\t" '{ print $19 }')
    # always total_RNA
    rna_material_type="total_RNA"
    echo -e "# Default RNA Material Type \t"$rna_material_type >> $idf
    # extraction date is not available
    rna_extraction_date=""
    echo -e "# Default RNA Extraction Date \t"$rna_extraction_date >> $idf
    # no storage
    rna_storage_protocol="NO STORAGE"
    echo -e "# Default RNA Storage Protocol \t"$rna_storage_protocol >> $idf
    # no storage, so there is no date
    rna_storage_date=""
    echo -e "# Default RNA Storage Date \t"$rna_storage_date >> $idf
    # no storage
    rna_storage_location="no storage"
    echo -e "# Default RNA Storage Location \t"$rna_storage_location >> $idf
    # always "SEQUENCING"
    seq_protocol="SEQUENCING"
    echo -e "# Default Sequencing Protocol \t"$seq_protocol >> $idf
    # performer is unknown, maybe sometimes manually
    seq_performer="other"
    echo -e "# Default Sequencing Performer \t"$seq_performer >> $idf
    # regex for instruments to get technology type
    # instrument information was extracted before in protocol section
    illumina=".*[iI][lL][lL][uU][mM][iI][nN][aA].*"
    solid=".*[sS][oO][lL][iI][dD].*"
    fourfivefour=".*454.*"
    pacific=".*[pP][aA][cC][iI][fF][iI][cC].*"
    if echo $seq_sequencing_instrument | grep -Eq "$illumina"
    then
        seq_technology_type="Sequencing by synthesis (Illumina)"
    elif echo $seq_sequencing_instrument | grep -Eq "$solid"
    then
        seq_technology_type="Sequencing by ligation (SOLiD sequencing)"
    elif echo $seq_sequencing_instrument | grep -Eq "$fourfivefour"
    then
        seq_technology_type="Pyrosequencing (454)"
    elif echo $seq_sequencing_instrument | grep -Eq "$pacific"
    then
        seq_technology_type="Single-molecule real-time sequencing (Pacific Bio)"
    else
        seq_technology_type="other"
    fi
    echo -e "# Default Sequencing Technology Type \t"$seq_technology_type >> $idf
    echo -e "# Default Sequencing Instrument \t"$seq_sequencing_instrument >> $idf
    # extracted library source
    seq_lib_source=$(echo "$line" | awk -F"\t" '{ print $22 }')
    echo -e "# Default Sequencing Library Source \t"$seq_lib_source >> $idf
    # extracted library strategy, wrong format, in best case there are 3 informations (single/paired, strand specific, polyA)
    seq_strategy=$(echo "$line" | awk -F"\t" '{ print $23 }')
    field1=$(echo "$seq_strategy" | awk -F'[;,]' '{ print $1 }')
    field2=$(echo "$seq_strategy" | awk -F'[;,]' '{ print $2 }')
    field3=$(echo "$seq_strategy" | awk -F'[;,]' '{ print $3 }')
    # single or paired end information should always be in the first field
    singlepaired="$field1"
    # test what information is in second field and formatting information
    case $field2 in
        "poly(A)")
            polya="poly-A filter";;
        "strand-specific")
            strand="strand specific";;
        *)
            polya="NA"
            strand="NA";;
    esac
    # test what information is in third field and formatting information
    case $field3 in
        "poly(A)")
            polya="poly-A filter";;
        "strand-specific")
            strand="strand specific";;
        *)
            polya="NA"
            strand="NA";;
    esac
    # assumption, if there is no information about poly-A filter, there is poly-A filter
    if [[ "$polya" == *NA* ]]
    then
        polya="poly-A filter"
    fi
    # assumption, if there is no information about strand-specificity, it is unspecific
    if [[ "$strand" == *NA* ]]
    then
        strand="unspecific"
    fi
    # formated library strategy
    seq_lib_strategy="$singlepaired;$strand;$polya"
    echo -e "# Default Sequencing Library Strategy \t"$seq_lib_strategy >> $idf
    # extracted library selection
    seq_lib_selection=$(echo "$line" | awk -F"\t" '{ print $24 }')
    echo -e "# Default Sequencing Library Selection \t"$seq_lib_selection >> $idf
    # library layout is unknown
    seq_lib_layout=""
    echo -e "# Default Sequencing Library Layout \t"$seq_lib_layout >> $idf
    # sequencing date is unknown
    seq_date=""
    echo -e "# Default Sequencing Date \t"$seq_date >> $idf
    # sequence is stored
    seq_storage_protocol="STORAGE"
    echo -e "# Default Sequencing Storage Protocol \t"$seq_storage_protocol >> $idf
    # date of download formatted to yyyy-mm-dd, if not empty
    seq_storage_date=$(echo "$line" | awk -F"\t" '{ print $28 }')
    if [[ $seq_storage_date == "" ]]
    then
        seq_storage_date=""
    else
        convert_date_full "$seq_storage_date"
        seq_storage_date=$year"-"$month"-"$day
    fi
    echo -e "# Default Sequencing Storage Date \t"$seq_storage_date >> $idf
    # downloaded on Uranos
    seq_storage_location="stored at HKI Jena"
    echo -e "# Default Sequencing Storage Location \t"$seq_storage_location >> $idf
    echo -e "# Default Data Processing Protocol \tQUALITY CONTROL OF RAW DATA" >> $idf
    echo -e "# Default Data Processing Software \t" >> $idf
    echo -e "# Default Data Processing Protocol Parameters and Values \t" >> $idf
    echo -e "# Default Data Processing Protocol \tQUALITY TRIMMING OF RAW DATA" >> $idf
    echo -e "# Default Data Processing Software \t" >> $idf
    echo -e "# Default Data Processing Protocol Parameters and Values \t" >> $idf
    echo -e "# Default Data Processing Protocol \tMAPPING" >> $idf
    echo -e "# Default Data Processing Software \t" >> $idf
    echo -e "# Default Data Processing Protocol Parameters and Values \t" >> $idf
    echo -e "# Default Data Processing Protocol \tCOUNTING" >> $idf
    echo -e "# Default Data Processing Software \t" >> $idf
    echo -e "# Default Data Processing Protocol Parameters and Values \t" >> $idf
    echo -e "# Default Data Processing Protocol \tDIFFERENTIAL EXPRESSION" >> $idf
    echo -e "# Default Data Processing Software \t" >> $idf
    echo -e "# Default Data Processing Protocol Parameters and Values \t" >> $idf
    echo -e "# Default Data Processing Storage Protocol \tSTORAGE" >> $idf
    echo -e "# Default Data Processing Storage Date \t" >> $idf
    echo -e "# Default Data Processing Storage Location \t" >> $idf
}


##########################################################################################################
##########################################################################################################

## Test if there is the right number of arguments supplied
if [ $# -eq 0 ]
then
    ## Output that there is not enough arguments supplied
    echo "no arguments supplied"
    ## Output how to use it
    echo "Usage: ./IDFSDRF.sh /full/Path/to/storage/directory \"/full/path/to/destination/directory\""
elif [ $# -eq 1 ]
then
    echo "not enough arguments supplied"
    echo "Usage: ./IDFSDRF.sh /full/Path/to/storage/directory \"/full/path/to/destination/directory\""
## Test if there is the right number of arguments supplied
elif [ $# -eq 2 ]
then
    # dir: given argument; full path to storage directory of downloaded information
    dir=$1

    # meta_dir: given argument; destination directory for storage of the metadata for experiments
    meta_dir=$2

    # meta_dir_test: just the last part of the path
    meta_dir_test=$(echo "$meta_dir" | awk -F"/" '{print $NF"/"}')

    # get into given $dir
    cd $dir

    # output: file for collecting all metadata of all storaged data in $dir
    #output=$dir"all_Metadata_sorted.tsv"
    output=$meta_dir"/all_Metadata_sorted.tsv"

    # if there is not already an "Experiment Metadata" directory
    if [ ! -d "$meta_dir" ]
    then
        # create $meta_dir
        mkdir $meta_dir
    fi

    # print header to collecting file
    echo -e "Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t RNA Extraction \t \t Sequencing \t \t \t \t \t \t \t \t" >> $output
    echo -e "GSE\tGSE Title\tPubmed ID\tGSE Status\tGSE Last Update\tGSE Summary\tGSE Overall Design\tGSE Author\tGSE Contact\tGSE Contributors\tGSM\tSpecimen\tCharacteristics\tStrain\tOrganism\tTaxID\tTreatment Protocol\tGrowth Protocol\tMaterial Type\tExtraction Protocol\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSRX Acc\tSRR Acc\tStorage Date\tData File" >> $output

    ## collect all metadata in one file
    # for all directories in $dir (organism folders)
    for dir_org in */;
    do
        # if $dir_org is not the destination directory
        if ! [[ $dir_org == *$meta_dir_test* ]]
        then
            # open $dir_org
            cd $dir_org
            # for all directories in $d (strain folders)
            for dir_strain in */;
            do
                # open $dir_strain
                cd $dir_strain
                # print all lines of sorted metadata table to output, except header
                awk -F"\t" 'NR>2 {print}' *Metadata_sorted.tsv >> $output
                # open $dir_organism
                cd ..
            done
            # open $dir
            cd ..
        fi
    done

    # GSE_list: extract all GSE from $output
    GSE_list=$(awk -F"\t" 'NR>2 { print $1 }' $output)
    # uniq_GSE: list with each GSE one time
    uniq_GSE=$(echo -e "$GSE_list" | sort -u)

    # open $meta_dir
    cd $meta_dir

    # nuniq_GSE: same as uniq_GSE but without GSE that are already there as directories (prevents overwriting)
    nuniq_GSE=""
    for GSE in $uniq_GSE
    do
        if [[ $(ls | grep -c *$GSE) == 0 ]]
        then
            nuniq_GSE=$(echo -e "$nuniq_GSE $GSE")
        fi
    done
    uniq_GSE=$nuniq_GSE

    ## split collected data into GSE files in GSE directories
    # for every GSE in $uniq_GSE
    for GSE in $uniq_GSE
    do
        # create $GSE directory
        mkdir $GSE
        # open $GSE directory
        cd $GSE
        # path: full path in the moment
        path=$(pwd)
        # output_GSE: path to created tsv file for $GSE
        output_GSE=$path"/"$GSE".tsv"
        # print header to output_GSE
        echo -e "Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t RNA Extraction \t \t Sequencing \t \t \t \t \t \t \t \t" >> $output_GSE
        echo -e "GSE\tGSE Title\tPubmed ID\tGSE Status\tGSE Last Update\tGSE Summary\tGSE Overall Design\tGSE Author\tGSE Contact\tGSE Contributors\tGSM\tSpecimen\tCharacteristics\tStrain\tOrganism\tTaxID\tTreatment Protocol\tGrowth Protocol\tMaterial Type\tExtraction Protocol\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSRX Acc\tSRR Acc\tStorage Date\tData File" >> $output_GSE
        # print all entries of collecting file to corresponding to GSE to output_GSE
        awk -F"\t" -v gse="$GSE" '{ if ($1 ~ gse) print $0}' OFS="\t" $output >> $output_GSE
        # open $meta_dir
        cd ..
    done

    ## convert to IDF and SDRF and rename all tables to organism_strain_GSE
    # for every directory in $meta_dir (GSE directories)
    for dir_GSE in */;
    do
        # if directory starts with "GSE"
        if [[ "$dir_GSE" == GSE* ]]
        then
            # open dir_GSE
            cd $dir_GSE
            # full path in the moment
            path=$(pwd)
            # GSE_file is the only file in this directory
            GSE_file=$(ls | grep *.tsv)
            # file: full path to GSE_file
            file=$path"/"$GSE_file
            # use function to_SDRF to transform $file to SDRF
            to_SDRF $file
            # use function to_IDF to transform $file to IDF
            to_IDF $file
            # strain: extracted strain of current GSE_file
            strain=$(awk -F"\t" 'NR>2 {print $14}' $GSE_file)
            # GSE: extracted GSE of current GSE_file
            GSE=$(awk -F"\t" 'NR>2 {print $1}' $GSE_file | sort -u)
            # if Dual Seq data
            if [[ $strain == *Dual-Seq* ]]
            then
                # organism: extracted organism of GSE_file
                organism=$(awk -F"\t" 'NR>2 {print $15}' $GSE_file)
                # orgs: unique, substitute empty spaces to underscore, if Dual-Seq combine the two organisms by "+", shorten organism to species
                orgs=$(echo -e "$organism" | sort -u | tr " " "_" | sed 's/,_/+/g' | sed 's/Cryptococcus_neoformans/neoformans/g' | sed 's/Candida_albicans/albicans/g' | sed 's/Candida_parapsilosis/parapsilosis/g' | sed 's/Mus_musculus/musculus/g' | sed 's/Homo_sapiens/sapiens/g' | sed 's/Aspergillus_fumigatus/fumigatus/g' | sed 's/Aspergillus_niger/niger/g' | sed 's/Aspergillus_flavus/flavus/g' | sed 's/Aspergillus_oryzae/oryzae/g' | sed 's/Candida_dubliniensis/dubliniensis/g' |sed 's/Candida_glabrata/glabrata/g' )
                # org: delete "-", substitute empty spaces to "--"
                org=$(echo -e $orgs | sed 's/-//g' | sed 's/ /--/g')
            else
                # organism: extracted organism of GSE_file
                organism=$(awk -F"\t" 'NR>2 {print $15"_"$14}' $GSE_file)
                # orgs: uniq, substitute empty spaces to underscore, shorten organism to species
                orgs=$(echo -e "$organism" | sort -u | tr " " "_" | sed 's/Cryptococcus_neoformans/neoformans/g' | sed 's/Candida_albicans/albicans/g' | sed 's/Candida_parapsilosis/parapsilosis/g' | sed 's/Mus_musculus/musculus/g' | sed 's/Homo_sapiens/sapiens/g' | sed 's/Aspergillus_fumigatus/fumigatus/g' | sed 's/Aspergillus_niger/niger/g' | sed 's/Aspergillus_flavus/flavus/g' | sed 's/Aspergillus_oryzae/oryzae/g' | sed 's/Candida_dubliniensis/dubliniensis/g' |sed 's/Candida_glabrata/glabrata/g' )
                # org: delete "-", substitute empty spaces to "--"
                org=$(echo -e $orgs | sed 's/-//g' | sed 's/ /--/g')
            fi
            # ntsv: name of the general tsv with organism and GSE
            ntsv=$org"_"$GSE".tsv"
            # sdrftsv: name of SDRF with organism and GSE
            sdrftsv=$org"_"$GSE"_SDRF.tsv"
            # idftsv: name of IDF with organism and GSE
            idftsv=$org"_"$GSE"_IDF.tsv"
            # rename $GSE_file to $ntsv
            mv $GSE_file $ntsv
            # rename SDRF file
            mv *"SDRF.tsv" $sdrftsv
            # rename IDF file
            mv *"IDF.tsv" $idftsv
            # open $meta_dir
            cd ..
        fi
    done

    ## get all new tables to $meta_dir, remove GSE directories
    # for every GSE_dir in $meta_dir (GSE directories)
    for GSE_dir in */;
    do
        # if GSE_dir starts with "GSE"
        if [[ "$GSE_dir" == GSE* ]]
        then
            # open $d
            cd $GSE_dir
            # move everything into $meta_dir
            mv * $meta_dir
            # open $meta_dir
            cd $meta_dir
            # remove $d
            rm -rf $GSE_dir
        fi
    done

    ## create appropriate directories with organism, strain and GSE information
    ## and move the corresponding files into these directories
    # for every GSE in $uniq_GSE
    for GSE in $uniq_GSE
    do
        # name: name of general tsv with organism and GSE without ".tsv"
        name=$(ls | grep "$GSE.tsv" | awk -F".tsv" '{print $1}')
        # if there is not already a directory with that name
        if ! [ -d $name ]
        then
            # create directory
            mkdir $name
            # move all related files with that GSE to the new directory
            mv *$GSE*"tsv" $name
        else
            mv *$GSE*"tsv" $name
        fi

    done

    # remove $output
    rm -rf $output
else
    # if there are to many arguments supplied
    echo "too many arguments supplied"
    echo "Usage: ./getMetaGSE.sh /full/Path/to/storage/directory \"/full/path/to/destination/directory\""
fi
