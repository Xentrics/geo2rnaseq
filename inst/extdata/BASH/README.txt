######## getMeta.sh ########
title			: getMeta.sh
description		: This script creates a metadata table for available
			  	  RNA-seq data in GEO (Gene Expression Omnibus) for
                  a given organism
author			: Silvia Mueller
date			: 2015/05/20
usage			: bash getMeta.sh "Organism" /full/Path/to/storage/directory
				  OR
				  ./getMeta.sh "Organism" /full/Path/to/storage/directory
usage example	: bash getMeta.sh "Candida albicans" /home/silvia/RNA-Seq/

######## getMetaGSE.sh ########
title			: getMetaGSE.sh
description		: This script creates a metadata table for available
			  	  RNA-seq data in GEO (Gene Expression Omnibus) for
                  a given GSE accession
author			: Silvia Mueller
date			: 2015/05/19
usage			: bash getMetaGSE.sh "GSE" /full/Path/to/storage/directory
				  OR
				  ./getMetaGSE.sh "GSE" /full/Path/to/storage/directory
usage example	: bash getMetaGSE.sh "GSE12345" /home/silvia/RNA-Seq/

######## getFastq.sh ########
title			: getFastq.sh
description		: This scripts downloads sequence read archive (sra)
				  files for all entries in given metadata table and
				  converts them to fastq format and gzip them with
				  highest compression
author			: Silvia Mueller
ate				: 2015/05/19
usage			: bash getFastq.sh /Path/to/metadata/table.tsv
				  OR
				  ./getFastq.sh /Path/to/metadata/table.tsv
usage example	: bash getFastq.sh /home/silvia/Aspergillus_fumigatus/CEA10/Aspergillus_fumigatus_CEA10_Metadata_sorted.tsv

######## IDFSDRF.sh ########
title			: IDFSDRF.sh
description		: This script creates a metadata table for available
			  	  RNA-seq data in GEO (Gene Expression Omnibus) for
                  a given organism
author			: Silvia Mueller
date			: 2015/04/10
usage			: bash IDFSDRF.sh /full/Path/to/storage/directory /full/path/to/destination/directory
				  OR
				  ./IDFSDRF.sh /full/Path/to/storage/directory "/full/path/to/destination/directory"
usage example	: bash IDFSDRF.sh /home/silvia/RNA-Seq/ "/home/silvia/RNA-Seq/Metadata"

