#!/bin/bash

## Informations
#  title            : getFastq.sh
#  description      : This scripts downloads sequence read archive (sra)
#                     files for all entries in given metadata table and
#                     converts them to fastq format and gzip them with
#                     highest compression
#  author           : Silvia Gerber (neé Mueller)
#  date             : 2016/10/13
#  usage            : bash getFastq.sh /Path/to/metadata/table.tsv
#  usage example    : bash getFastq.sh /home/silvia/Aspergillus_fumigatus/CEA10/Aspergillus_fumigatus_CEA10_Metadata_sorted.tsv

# test if argument
if [ $# -eq 0 ]
then
    echo "No arguments supplied"
# test if one argument (should be path to metafile)
elif [ $# -eq 1 ]
then
    # metafile as argument
    metafile="$1"
    # pathmeta: path to the file without filename
    # pathmeta=$(echo "$metafile" | awk -F"/" 'sub(FS $NF,x)')
    pathmeta=$(dirname $metafile)

    # enter into directory
    cd $pathmeta 2>/dev/null

    # create tmp folder
    mkdir "tmp" 2>/dev/null

    # create temporary help files
    tmpa=tmp/tmpa.txt
    tmpb=tmp/tmpb.txt
    fastqlog=tmp/fastq-dump.log
    today=$(date +"%Y-%m-%d")
    error=$pathmeta"/getFastq_error_"$today".log"

    # list of SRR (uniq)
    listOfSRR=$(awk -F"\t" 'NR>2 { print $27 }' OFS="\t" $metafile | sort -V | uniq)

    # for every SRR
    for srr in $listOfSRR
    do
        # first 6 characters of SRR
        f6=$(echo $srr | cut -c -6)

        # build SRA URL
        url="ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/"$f6"/"$srr"/"$srr".sra"

        # extract important data
        # $1=gse;  $11=gsm;  $8=gse author; $4=gse status (year); $25=Layout
        awk -v srr="$srr" -F'\t' '{ if ($27 ~ srr){print $1">"$11">"$8">"$4">"$23}}' $metafile >> $tmpa

        # delete multiple entries
        cat $tmpa | sort -V | uniq > $tmpb

        # for every line in sorted (uniq) file
        while read line
        do
            # GSE
            gse=$(echo $line | awk -F">" '{ print $1 }' | tr -d " ")
            if [ -z "$gse" ]
            then
                gse="GSEunavailable"
            fi

            # GSM
            gsm=$(echo $line | awk -F">" '{ print $2 }' | tr -d " ")
            if [ -z "$gsm" ]
            then
                gsm="GSMunavailable"
            fi

            # Author
            author=$(echo $line | awk -F">" '{ print $3 }' | tr -d " ")
            if [ -z "$author" ]
            then
                author="AUTHORunavailable"
            fi

            # Year
            year=$(echo $line | awk -F">" '{ print $4 }'| awk '{ print $5 }' | tr -d " " | tr -d ";")
            if [ -z "$year" ]
            then
                year="YEARunavailable"
            fi

            # Layout (single, paired)
            layout=$(echo $line | awk -F">" '{ print $5 }')
            if [ -z "$layout" ]
            then
                layout="LAYOUTunavailable"
            fi

            # build filename
            filename=$author"_"$year"_"$gse"_"$gsm"_"$srr".sra"

            # basic path
            path=$(pwd)

            # test if there is already a file with this SRR
            cnt=$(ls *$srr*.fastq.gz 2>/dev/null | wc -l)
            if [ "$cnt" != "0" ]
            then
                # copy path from other entry
                path=$(awk -F"\t" '{ print $29 }' $metafile | grep -m 1 $srr)
                dtime=$(awk -F"\t" '{ print $28 }' $metafile | grep -m 1 $srr)
                awk -v path="$path" -v dtime="$dtime" -v srr="$srr" -F"\t" '{ if($27 ~ /srr/){$28=dtime; $29=path}; print}' OFS="\t" $metafile > temp && mv temp $metafile
            else
                # download .sra
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    wget -q -O "$filename" $url
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # fastq-dump (--split-3: will save forward and reverse reads in separate files. If the file contains only
                # the single end read, only one fastq file will be generated)
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    fastq-dump --split-3 $filename >> $fastqlog
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # gzip fastq
                RET=1
                until [ "$RET" -eq 0 ]
                do
                    pigz -9 -p50 *.fastq
                    RET=$?
                    if [ "$RET" -ne 0 ]
                    then
                        sleep 10
                    fi
                done

                # building path to fastq files and insert them into table
                # set starting point for path
                path=$(pwd)
                # routine applies if data is paired-end
                if [[ $layout == *"paired"* ]]
                then
                    # build new path for insert into table
                    path=$path"/"$author"_"$year"_"$gse"_"$gsm"_"$srr"_1.fastq.gz; "$path"/"$author"_"$year"_"$gse"_"$gsm"_"$srr"_2.fastq.gz"
                    awk -v path="$path" -v date="$(LANG="en_UK.UTF.8" date)" -v srr="$srr" -F"\t" '{ if ($27 ~ srr){$28=date; $29=path}; print }' OFS="\t" $metafile > temp && mv temp $metafile
                # routine applies if data is single-end
                elif [[ $layout == *"single"* ]]
                then
                    # build new path for insert into table
                    path=$path"/"$author"_"$year"_"$gse"_"$gsm"_"$srr".fastq.gz"
                    # insert path to file into table
                    awk -v path="$path" -v date="$(LANG="en_UK.UTF.8" date)" -v srr="$srr" -F"\t" '{ if ($27 ~ srr){$28=date; $29=path}; print }' OFS="\t" $metafile > temp && mv temp $metafile
                # routine applies if layout is NA
                elif [[ $layout == *"NA"* ]]
                then
                    # build new path for insert into table
                    path=$path"/"$author"_"$year"_"$gse"_"$gsm"_"$srr".fastq.gz"
                    # insert path to file into table
                    awk -v path="$path" -v date="$(LANG="en_UK.UTF.8" date)" -v srr="$srr" -F"\t" '{ if ($27 ~ srr){$28=date; $29=path}; print }' OFS="\t" $metafile > temp && mv temp $metafile
                # applies for all other cases (pretty unlikely)
                else
                    exit "Something went wrong"
                fi

                # remove .sra
                rm -rf $filename
            fi
        done < $tmpb


        # remove temporary files
        rm -rf $tmpa 2>/dev/null
        rm -rf $tmpb 2>/dev/null
        rm -rf $fastqlog 2>/dev/null
    done
# more than one argument
else
    echo "too many arguments"
fi

# remove all temporary files and directories
rm -rf tmp 2>/dev/null
