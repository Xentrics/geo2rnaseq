#!/bin/bash

## Informations
#  title            : getMetaGSE.sh
#  description      : This script creates a metadata table for available
#                     RNA-seq data in GEO (Gene Expression Omnibus) for
#                     a given GSE accession
#  author           : Silvia Mueller
#  date             : 2015/05/19
#  usage            : bash getMetaGSE.sh "GSE" /full/Path/to/storage/directory
#  usage example    : bash getMetaGSE.sh "GSE12345" /home/silvia/RNA-Seq/

# NCBI search term example for Homo + x but not dual RNA-seq:
# "Homo sapiens"[Organism] AND "Candida albicans"[All Fields] AND "Expression profiling by high throughput sequencing"[Filter]

## keep temp output for debugging?
debug=0

## Date of today
#  today: contains date of today in YYYY/MM/DD
today=$(date +"%Y/%m/%d")

## Test if there is the right number of arguments supplied
if [ $# -eq 0 ]
then
    ## Output that there is not enough arguments supplied
    echo "no arguments supplied"
    ## Output how to use it
    echo "Usage: ./getMetaGSE.sh \"GSE\" /full/path/to/storage/directory"
elif [ $# -eq 1 ]
then
    echo "not enough arguments supplied"
    echo "Usage: ./getMetaGSE.sh \"GSE\" /full/path/to/storage/directory"
## Test if there is the right number of arguments supplied
elif [ $# -eq 2 ]
then
    outputpath=$2
    cd $outputpath

    ## Transformations of the input
    GSE=$1

    ## Basic URLs
    # basicURLeUtils: basic URL for using the eutils
    basicURLeUtils="eutils.ncbi.nlm.nih.gov/entrez/eutils/"
    # basicURLGEO: basic URL for query in GEO
    basicURLGEO="ncbi.nlm.nih.gov/geo/query"

    mkdir "$GSE" 2>/dev/null
    cd "$GSE"

    # directory: path to current directory
    directory=$(pwd)
    # outputfile: manipulating the original output path, insert strain
    outputfile=$directory"/"$GSE"_Metadata_sorted.tsv"

    url_gse=$basicURLGEO"/acc.cgi?targ=gsd&acc="$GSE"&form=text"
    outputGSE=$GSE".txt"
    RET=1
    until [ "$RET" -eq 0 ]
    do
        wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputGSE "$url_gse"
        RET=$?
        if [ "$RET" -ne 0 ]
        then
            sleep 10
        fi
    done

    typeIsArray=$(grep "\!Series_type" $outputGSE | grep -c "profiling by array")
    if [[ $typeIsArray == 1 ]]
    then
        echo "ERROR: this is a microarray experiment, not RNA-seq!"
        exit 1
    fi

    listOfGSM=$(grep "GSM" < $outputGSE | awk -F"Series_sample_id = " '{print $2 }')

    ## Getting GSM information files
    #  for every GSM accession in list
    #echo -e $listOfGSM
    for GSM_acc in $listOfGSM
    do
        GSM_acc=$(echo -e "${GSM_acc}" | tr -d '[[:space:]]')
        # URL for retrieving the GSM information file
        url_gsm=$basicURLGEO"/acc.cgi?targ=gsm&acc="$GSM_acc"&form=text"

        # c1 is a helping variable, which contains number of sample organisms in GSM information file
        RET=1
        until [ "$RET" -eq 0 ]
        do
            c1=$(wget -q "$url_gsm" -O - | grep "!Sample_organism" | wc -l)
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        # create GSM directory and enter it
        mkdir "$GSM_acc" 2>/dev/null
        cd "$GSM_acc"

        # outputTXTGSM: name of the output file for GSM information
        outputTXTGSM=$GSM_acc".txt"

        # getting GSM information file
        RET=1
        until [ "$RET" -eq 0 ]
        do
            wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputTXTGSM "$url_gsm"
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        # extract strain to strain list
        # the variable stamm is used here for building this list
        # characteristics: extracted characteristics of GSM information file
        characteristics=$(grep "!Sample_characteristics_ch1" < $outputTXTGSM | cut -c31- | tr '\r' ";")
        # test if characteristics contains the word "strain"
        if [[ $c1 == 1 ]]
        then
            if [[ $characteristics == *strain* ]]
            then
                # if characteristics contain the word "strain"
                # c is a helping variable for counting the number of occurences of the word "strain" in characteristics
                c=$(echo $characteristics | grep -oE "strain" | wc -l)
                # s1 is a helping variable
                s1=$organism" strain:"
                # s2 is a helping variable
                s2=$organism" strain background:"
                # r is a regex for forms of "strain" with brackets before and after the word
                r=".*\(.*strain\).*"
                # test if there is just one occurence of the word "strain" in characteristics
                if [[ $c -eq 1 ]]
                then
                    # if there is just one occurence of the word "strain"
                    # test if the regex pattern is in characteristics
                    if echo $characteristics | grep -Eq "$r"
                    then
                        # if the regex pattern is in characteristics
                        # st is a helping variable, containing everything with the "strain" together in brackets
                        st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                        # st1 is a helping variable, containing the first word in brackets of st
                        st1=$(echo $st | awk '{ print $1 }')
                        # test if st1 contains the word "background"
                        if [[ "$st1" == *background* ]]
                        then
                            # if st1 contains the word "background"
                            # stamm is associated with a string, suggesting you have to look into characteristics, because there is no distinct strain named
                            stamm="look into characteristics"
                        else
                            # if st1 does not contain the word "background"
                            # stamm is this strain
                            stamm=$(echo $st1)
                        fi
                    # test if the pattern s1 is in characteristics
                    elif [[ $characteristics == *$s1* ]]
                    then
                        # if pattern s1 is in characteristics
                        # stamm is the word after pattern s1 until next ";"
                        stamm=$(echo $characteristics | awk -v s1="$s1" -F"$s1 " '{ print $2 }' | awk -F";" '{ print $1 }')
                    # test if the pattern s2 is in characteristics
                    elif [[ $characteristics == *$s2* ]]
                    then
                        # if pattern s2 is in characteristics
                        # stamm is the word after pattern s2 until next ";"
                        stamm=$(echo $characteristics | awk -v s2="$s2" -F"$s2 " '{ print $2 }' | awk -F";" '{ print $1 }')
                    else
                        # if no pattern is in characteristics
                        # stamm is the word after "strain: " until the next ";"
                        stamm=$(echo $characteristics | awk -F"strain: " '{ print $2 }' | awk -F";" '{ print $1 }')

                        # if stamm is still empty but characteristics contains strain/background
                        if [[ $stamm == "" && $characteristics == *"strain/background: "* ]]
                        then
                            # stamm: contains everything after the string "strain/background :" until the next ";"
                            stamm=$(echo $characteristics | awk -F"strain/background: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                        fi
                    fi
                fi
            else
                # if there is no occurence or more than one occurence of the word "strain" in characteristics
                # stamm is not availabe
                stamm="NA"
            fi
        elif [[ $c1 == 2 ]]
        then
            stamm="Dual-Seq"
        fi

        # stamm is trimmed of whitespaces
        stamm=$(echo $stamm | tr -d " ")

        # strains: list of strains
        # strains is appended by stamm
        strains=$strains" "$stamm


        # srx_acc: SRX accession of the GSM information file
        # srx_acc=$(grep "sra-instant" < $outputTXTGSM | awk -F"/SRX/" '{ print $2 }' | awk -F"/" '{ print $2 }' | tr -d '\r')
        srx_acc=$(grep -oP 'sra\?term=\K\w+' $outputTXTGSM | tr -d '\r')
        # url_srxID: URL for retrieving SRX IDs
        url_srxID=$basicURLeUtils"esearch.fcgi?db=sra&term="$srx_acc"&retmax=10000"

        # srx_id: extracted SRX ID
        RET=1
        until [ "$RET" -eq 0 ]
        do
            srx_id=$(wget -q "$url_srxID" -O - | grep "<Id>" | rev | cut -c6- | rev | sed 's/<Id>//g')
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        # get SRX metadata
        # url_srx: URL for retrieving SRX information file
        url_srx=$basicURLeUtils"efetch.fcgi?db=sra&id="$srx_id"&retmax=10000"
        # outputSRX: name of output file for SRX information
        outputSRX=$srx_acc".xml"
        # getting SRX information
        RET=1
        until [ "$RET" -eq 0 ]
        do
            wget -q --retry-connrefused --tries=inf --timeout=10000 --random-wait -O $outputSRX "$url_srx"
            RET=$?
            if [ "$RET" -ne 0 ]
            then
                sleep 10
            fi
        done

        # change to GSE directory
        cd ".."
        # echo $(pwd)
    done

    ##########################################
    ## Starting Building the Metadata table ##
    ##########################################

    # for every folder in GSE directory (GSM folders)
    for d in $(ls -d */);
    do
        echo "-------------------------------------------------"

        # GSM: name GSM folder and also the GSM accession number
        GSM=$(echo $d|awk -F"/" '{print $1}')
        # change to GSM folder
        cd "$GSM"
        # GSM_file: name of the GSM file
        GSM_file=$GSM".txt"
        echo "GSM file: $GSM_file"

        # gse: GSE accession
        gse=$(pwd | awk -F"/" '{ print $(NF-1) }')

        # gse_file: GSE information file
        gse_file="../$gse.txt"

        # gse_title: Series Title in GSE file
        gse_title=$(grep "!Series_title" < $gse_file | awk -F" = " '{ print $2 }' | tr "\r" ";" | tr "\n" ";" | tr -s ";")

        # pubmed: Pubmed ID (sometimes NA)
        # test if pubmed ID is available in GSE file
        if grep -q "!Series_pubmed_id" < $gse_file
        then
            # if pubmed ID is availale, pubmed is assigned this ID
            pubmed=$(grep "!Series_pubmed_id" < $gse_file | awk -F" = " '{ print $2 }' |tr "\r" ";"| tr "\n" ";"| tr -s ";")
        else
            # if no pubmed ID available, pubmed is NA
            pubmed="NA"
        fi

        # gse_status: Series status in GSE file
        gse_status=$(grep "!Series_status" < $gse_file | awk -F" = " '{ print $2 }' | tr "\r" ";"| tr "\n" ";"| tr -s ";")

        # gse_lastupdate: Series last update in GSE file
        gse_lastupdate=$(grep "!Series_last_update" < $gse_file | awk -F" = " '{ print $2 }'| tr "\r" ";"| tr "\n" ";"| tr -s ";")

        # gse_summary: Series summary in GSE file
        gse_summary=$(grep "!Series_summary" < $gse_file | awk -F" = " '{ print $2 }' |tr "\r" ";"| tr "\n" ";"| tr -s ";")

        # gse_overall: Series overall design in GSE file
        gse_overall=$(grep "!Series_overall_design" < $gse_file | awk -F" = " '{ print $2 }'| tr "\r" ";"| tr "\n" ";"| tr -s ";")

        # gse_contact: Series Contact (seperated by ;) in GSE file
        gse_contact=$(grep "!Series_contact_" < $gse_file | awk -F"Series_contact_" '{ print $2 }' | awk -F" = " '{ print $1" : "$2 }' | tr '\r' ";"| tr "\n" ";"| tr -s ";" | sed 's/;;/;/')
        # gse_author: extracted name from gse_contact
        gse_author=$(echo $gse_contact| awk -F"name : " '{ print $2 }' | awk -F";" '{ print $1 }' | sed 's/,/_/' | sed 's/,/_/' | sed 's/__/_/' | sed 's/_//')
        # if there is the flag "Series Contributors" in GSE file
        if grep -q "!Series_contributor" < $gse_file
        then
            # gse_contrib: Series contributors
            gse_contrib=$(grep "!Series_contributor" < $gse_file | awk -F" = " '{ print $2 }' | awk -F",," '{ print $1" "$2 }' | tr '\r' ";"| tr "\n" ";"| tr -s ";")
        else
            # gse_contrib: not available
            gse_contrib="NA"
        fi

        # specimen: Sample title from GSM file
        # specimen=$(grep "!Sample_title" < $GSM_file | cut -c11- | tr -d '\r')
        specimen=$(grep -oP '\!Sample_title = \K.*' $GSM_file | tr -d '\r')
        echo "Specimen: $specimen"

        # characteristics: Sample characteristics from GSM file
        characteristics=$(grep "!Sample_characteristics_ch1" < $GSM_file | cut -c31- | tr '\r' ";"| tr "\n" ";"| tr -s ";")
        echo "Characteristics: $characteristics"
        # organism
        organism=$(grep "!Sample_organism_ch1" < $GSM_file | awk -F"Sample_organism_ch1 = " '{ print $2}' | tr -d "\r")
        echo "Organism: $organism"

        c1=$(grep "!Sample_organism" < $GSM_file | wc -l)

        if [[ $c1 == 1 ]]
        then
            # test if characteristics contains strain information
            if [[ $characteristics == *strain* ]]
            then
                # c is a help variable counting the number of occurences of the word "strain" in the characteristics variable
                c=$(echo $characteristics | grep -oE "strain" | wc -l)
                # s1 is a help variable as pattern for example "Candida albicans strain:"
                s1=$organism" strain:"
                # s2 is a help variable as pattern for example "Candida albicans strain background:"
                s2=$organism" strain background:"
                # r is a regex pattern for strain related information in brackets, like "(SC5314 strain)"
                r=".*\(.*strain\).*"
                # test if there is just one occurence of word "strain"
                if [[ $c -eq 1 ]]
                then
                    # test if regex pattern r is in characteristics like "(background strain)" or "(SC5314 strain)"
                    if echo $characteristics | grep -Eq "$r"
                    then
                        # st is a help variable containing the words between brackets eg "background strain" or "SC5314 strain"
                        st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                        # st1 is a help variable containing the first word in brackets eg "background" or "SC5314"
                        st1=$(echo $st | awk '{ print $1 }')
                        # test if st1 contains the word "background"
                        if [[ "$st1" == *background* ]]
                        then
                            # if it contains "background" there is probabily some information in characteristics which can be retrieved manually
                            strain="look into characteristics"
                        else
                            # if it does not contains "background" the first word is most likely the name of the strain
                            strain=$(echo $st1)
                        fi
                    else
                        # if the regex pattern r is not found
                        # strain: contains everything after the string "strain :" until the next ";"
                        strain=$(echo $characteristics | awk -F"strain: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")

                        # if strain is still empty but characteristics contains strain/background
                        if [[ $strain == "" && $characteristics == *"strain/background: "* ]]
                        then
                            # strain: contains everything after the string "strain/background :" until the next ";"
                            strain=$(echo $characteristics | awk -F"strain/background: " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                        fi
                    fi
                else
                    # if there are more occurences of the word "strain"
                    # s1 is a help variable as pattern for example "Candida albicans strain:"
                    s1=$organism" strain:"
                    # test if characteristics contains the string s1
                    if [[ $characteristics == *$s1* ]]
                    then
                        # strain: everything after string s1 and before next ";"
                        strain=$(echo $characteristics | awk -v s1="$s1" -F"$s1 " '{ print $2 }' | awk -F";" '{ print $1 }'| tr -d " ")
                    else
                        # s2 is a help variable as pattern for example "Candida albicans strain background:"
                        s2=$orga" strain background:"
                        # test if characteristics contains the string s2
                        if [[ $characteristics == *$s2* ]]
                        then
                            # strain: everything after string s2 and before next ";"
                            strain=$(echo $characteristics | awk -v s2="$s2" -F"$s2 " '{ print $2 }' | awk -F";" '{ print $1 }' | tr -d " ")
                        else
                            # strain: can manually be retrieved
                            strain="look into characteristics"
                        fi
                    fi
                fi
            # test if characteristics contain the string "strain)"
            elif [[ $characteristics == *"strain)"* ]]
            then
                # st is a help variable containing everything between the brackets
                st=$(echo $characteristics | awk -F"(" '{ print $2 }' | awk -F")" '{ print $1 }')
                # st1 is a help variable containing the first word in the brackets
                st1=$(echo $st | awk '{ print $1 }')
                # test if st1 contains the string "background"
                if [[ "$st1" == *background* ]]
                then
                    # strain: strain information can possibly manually retrieved
                    strain="look into characteristics"
                else
                    # strain: st1 is most likely the name of the strain
                    strain=$(echo $st1 | tr -d " ")
                fi
            else
                # strain: not available
                strain="NA"
            fi
        elif [[ $c1 == 2 ]]
        then
            strain="Dual-Seq"
        fi
        # strain_f: trimming white spaces out of strain
        strain_f=$(echo $strain | tr -d " ")
        echo "Strain: $strain_f"

        # org: Sample organism ch1 in GSM file
        org=$(grep "!Sample_organism" < $GSM_file | cut -c24- | tr '\r' ";")

        # taxid: sample taxid ch1 in GSM file
        taxid=$(grep "!Sample_taxid_ch1" < $GSM_file | cut -c21- | tr '\r' ";")
        echo "Taxon ID: $taxid"

        # test if "!Sample_treatment_protocol" information can be found in GSM file
        if grep -q "!Sample_treatment_protocol" < $GSM_file
        then
            # if there is information available
            # treat: containing treatment protocol from GSM file
            treat=$(grep "!Sample_treatment_protocol" < $GSM_file | awk -F " = " '{ print $2 }' | tr '\r' ";"| tr "\n" ";")
        else
            # if there is no information available
            # treat: not available
            treat="NA"
        fi

        echo "Treatment: $treat"

        # test if "!Sample_growth_protocol" information can be found in GSM file
        if grep -q "!Sample_growth_protocol" < $GSM_file
        then
            # if there is information available
            # growth: containing growth protocol from GSM file
            growth=$(grep "!Sample_growth_protocol_ch1" < $GSM_file | cut -c31- | tr '\r' ";"| tr "\n" ";")
        else
            # if there is no information available
            # growth: not available
            growth="NA"
        fi

        echo "Growth protocol: $growth"

        # mt: Material Type (total RNA, polyA RNA ...) from GSM file
        mt=$(grep "!Sample_molecule_ch1" < $GSM_file | cut -c24- | tr -d '\r')
        echo "Material type: $mt"

        # extraction: Extraction protocol from GSM file
        extraction=$(grep "!Sample_extract_protocol_ch1" < $GSM_file | cut -c32- | tr '\r' ";"| tr "\n" ";")
        echo "Extraction protocol: $extraction"

        # instrument: sequencing instrument model from GSM file
        instrument=$(grep "!Sample_instrument_model" < $GSM_file | cut -c28- | tr -d '\r')
        echo "Sequencing instrument: $instrument"

        # libsource: Library Source from GSM file (mostly transcriptomic)
        libsource=$(grep "!Sample_library_source" < $GSM_file | cut -c26- | tr -d '\r')
        echo "Library source: $libsource"

        # libselection: Library selection from GSM file (like cDNA)
        libselection=$(grep "!Sample_library_selection" < $GSM_file | cut -c29- | tr -d '\r')
        echo "Library selection: $libselection"

        # srx_acc: extracted SRX accession from GSM file
        # srx_acc=$(grep "sra-instant" < $GSM_file | awk -F"/SRX/" '{ print $2 }' | awk -F"/" '{ print $2 }' | tr -d '\r')

        # example line in GSM file: "!Sample_relation = SRA: https://www.ncbi.nlm.nih.gov/sra?term=SRX403442"
        srx_acc=$(grep -oP 'sra\?term=\K\w+' $GSM_file | tr -d '\r')
        echo "SRX: $srx_acc"

        # srxfile: name of information file for SRX Accession
        srxfile=$srx_acc".xml"
        # liblayout: here just extracted information between flags in information file from SRX accession
        liblayout=$(grep "LIBRARY_LAYOUT" < $srxfile | awk -F"<LIBRARY_LAYOUT>" '{ print $2 }' | awk -F"</LIBRARY_LAYOUT>" '{ print $1 }' | tr -cd '[:alpha:]')
        echo $liblayout

        # formatting output of liblayout
        # if liblayout contains the string "PAIRED"
        if [[ $liblayout == *PAIRED* ]]
        then
            # liblayout: is paired end
            liblayout="paired end"
        # if liblayout contains the string "SINGLE"
        elif [[ $liblayout == *SINGLE* ]]
        then
            # liblayout: is single end
            liblayout="single end"
        fi

        echo "Library layout: $liblayout"

        # seq: extracted library strategy from GSM file ( mostly RNA Seq )
        # attention: library strategy in GEO is something totally different from library strategy in FungiNet
        seq=$(grep "!Sample_library_strategy" < $GSM_file | cut -c28- | tr -d '\r')
        echo "Library strategy: $seq"

        # Library Strategy
        # polyregex: REGEX pattern for recognition whether just mRNA was extracted
        # polyregex matches the following
        # mRNA, polyadenylated, Polyadenylated, poly-adenylated, poly-A, poly-a, Poly-A, Poly-a, Poly A, Poly a,
        # poly A, poly a, Ribo-Minus, Ribo Minus, RiboMinus, Ribominus, Ribo-minus, Ribo minus, RiboZero, Ribozero,
        # Ribo Zero, Ribo zero, Ribo-Zero, Ribo-zero, Poly(A), poly(A), poly(a), Poly(a), Poly (A), Poly (a),
        # poly (A), poly (a), Poly-(A), Poly-(a), poly-(A), poly-(a), PolyA, polyA, polya, Polya
        polyregex=".*(mRNA|[pP]oly[- ]?(?:adenylated|[Aa]\s+|\([Aa]\))\s+|Ribo[- ]?[mM]inus|Ribo[- ]?[zZ]ero).*"
        # test if the regex can be found in extraction information
        if echo $extraction | grep -Eq "$polyregex"
        then
            # if the regex can be found in extraction information
            # polyA: is positiv
            polyA="yes"
        # test if the regex can be found in material type information
        elif echo $mt | grep -Eq "$polyregex"
        then
            # if the regex can be found in material type information
            # polyA: is positiv
            polyA="yes"
        # test if the regex can be found in characteristics
        elif echo $characteristics | grep -Eq "$polyregex"
        then
            # if the regex can be found in characteristics
            # polyA: is positiv
            polyA="yes"
        else
            # if the regex can not be found anywhere
            # polyA: information is not available
            polyA="NA"
        fi

        # strandregex: regex for different notations of strand-specific
        strandregex=".*([sS]trand[- ]?specific).*"
        # nonstrand: regex for different notations of non-strand-specific
        nonstrand=".*(non[- ]?[sS]trand[- ]?specific).*"
        # nonstrandregex: regex for different notations of strand-non-specific
        nonstrandregex=".*([sS]trand[- ]?non[- ]?specific).*"
        # test if extraction contains strandregex AND nonstrand
        if (echo $extraction | grep -Eq "$strandregex") && (echo $extraction | grep -Eqv "$nonstrand")
        then
            # test if extraction contains nonstrandregex (because there could be conflicting informations in freetext)
            if echo $extraction | grep -Eq "$nonstrandregex"
            then
                # strand: double (means, that there is a double match and resulting conflicting informations)
                strand="double"
            # test if characteristics contains nonstrandregex (because there could be conflicting informations in freetext)
            elif echo $characteristics | grep -Eq "$nonstrandregex"
            then
                # strand: double (means, that there is a double match and resulting conflicting informations)
                strand="double"
            else
                # if nonstrandregex can not be found, there is most likely no confliciting informations
                # strand: yes (it is strand specific)
                strand="yes"
            fi
        # test if characteristics contains strandreged AND nonstrand
        elif (echo $characteristics | grep -Eq "$strandregex") && (echo $characteristics | grep -Eqv "$nonstrand")
        then
            # test if extraction contains nonstrandregex (because there could be conflicting informations in freetext)
            if echo $extraction | grep -Eq "$nonstrandregex"
            then
                # strand: double (means, there is a double match and resulting conflicting informations)
                strand="double"
            # test if characteristics contains nonstrandregex (because there could be conflicting informations in freetext)
            elif echo $characteristics | grep -Eq "$nonstrandregex"
            then
                # strand: double (means, there is a double match and resulting conflicting informations)
                strand="double"
            else
                # strand: yes (it is strand specific)
                strand="yes"
            fi
        # test if extraction contains nonstrandregex
        elif echo $extraction | grep -Eq "$nonstrandregex"
        then
            # strand: no (it is non-strand-specific)
            strand="no"
        # test if characteristics contains nonstrandregex
        elif echo $characteristics | grep -Eq "$nonstrandregex"
        then
            # strand: no (it is non-strand-specific)
            strand="no"
        else
            # if nothing matchs
            # strand: not available
            strand="NA"
        fi

        # test if polyA is yes
        if [[ "$polyA" == "yes" ]]
        then
            # test if strand is yes
            if [[ "$strand" == "yes" ]]
            then
                # libstrategy: liblayaout (single/paired end); poly(A), strand-specific
                libstrategy=$liblayout"; poly(A); strand-specific"
            # test if strand is no
            elif [[ "$strand" == "no" ]]
            then
                # libstrategy: liblayout (single/paired end); poly(A), strand-non-specific
                libstrategy=$liblayout"; poly(A); strand-non-specific"
            # test if strand is double
            elif [[ "$strand" == "double" ]]
            then
                # libstrategy: liblayout (single/paired end); poly(A), manual information for strand
                libstrategy=$liblayout"; poly(A); look into characteristics and/or extraction for strand information"
            else
                # no information about strand
                # libstrategy: liblayout (single/paired end); poly(A)
                libstrategy=$liblayout"; poly(A)"
            fi
        # test if polyA is NA
        elif [[ "$polyA" == "NA" ]]
        then
            # test if strand is yes
            if [[ "$strand" == "yes" ]]
            then
                # libstrategy: liblayout (single/paired end); strand specific
                libstrategy=$liblayout"; strand-specific"
            # test if strand is no
            elif [[ "$strand" == "no" ]]
            then
                # libstrategy: liblayout (single/paired end); strand-non-specific
                libstrategy=$liblayout"; strand-non-specific"
                # test if strand is double
            elif [[ "$strand" == "double" ]]
            then
                # libstrategy: liblayout (single/paired end); manual search for strand information
                libstrategy=$liblayout"; look into characteristics and/or extraction for strand information"
            else
                # no information about strand
                # libstrategy: liblaylout (single/paired end)
                libstrategy=$liblayout
            fi
        fi

        # liblayout as in FungiNet is not available
        #liblayout="NA"

        # srr: SRR accession
        srr=$(grep "RUN_SET" < $srxfile | awk -F"<RUN_SET>" '{ print $2 }' | awk -F"<PRIMARY_ID>" '{ print $2 }' | awk -F"</PRIMARY_ID>" '{ print $1 }')
        echo "SRR: $srr"

        # seqstoragedate: Storage Date (empty, because no download till now)
        seqstoredate=""

        # seqstoragelocation: Storage Path (empty, because no download till now)
        seqstorelocation=""

        # just output if SRR available
        if [[ $srr == *SRR* ]]
        then
            # writing all information to outputfile
            echo -e "$gse\t$gse_title\t$pubmed\t$gse_status\t$gse_lastupdate\t"$gse_summary"\t$gse_overall\t"$gse_author"\t"$gse_contact"\t"$gse_contrib"\t$GSM\t$specimen\t"$characteristics"\t$strain\t"$organism"\t"$taxid"\t$treat\t$growth\t$mt\t"$extraction"\t$instrument\t$libsource\t$libstrategy\t$libselection\t$liblayout\t$srx_acc\t$srr\t$seqstoredate\t$seqstorelocation" >> $outputfile
        fi

        # get into GSE folder
        cd ".."

    done

    if [ -s $outputfile ]
    then
        sed -i '1s/^/GSE\tGSE Title\tPubmed ID\tGSE Status\tGSE Last Update\tGSE Summary\tGSE Overall Design\tGSE Author\tGSE Contact\tGSE Contributors\tGSM\tSpecimen\tCharacteristics\tStrain\tOrganism\tTaxID\tTreatment Protocol\tGrowth Protocol\tMaterial Type\tExtraction Protocol\tSequencing Instrument\tLibrary Source\tLibrary Strategy\tLibrary Selection\tLibrary Layout\tSRX Acc\tSRR Acc\tStorage Date\tData File\n/' $outputfile
        sed -i '1s/^/Source \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t \t RNA Extraction \t \t Sequencing \t \t \t \t \t \t \t \t\n/' $outputfile
    fi

    # remove temp or keep for debugging?
    if [[ $debug == 0 ]]
    then
	    rm -rf $GSE.txt
	    rm -rf GSM*
    fi

else
    # if there are to many arguments supplied
    echo "too many arguments supplied"
fi
